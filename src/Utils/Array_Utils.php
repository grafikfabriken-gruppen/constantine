<?php

namespace GF\Utils;

class Array_Utils
{

    /**
     * Serialize object or array into array!
     *
     * @param array|object $obj
     * @return array
     */
    public static function serialize($obj)
    {

        $return = is_object($obj) ? get_object_vars($obj) : (array_has_items($obj) ? $obj : []);

        if (array_has_items($return)) {
            foreach ($return as $key => $value) {
                if (is_object($value) || array_has_items($value)) {
                    $return[$key] = self::serialize($value);
                }
            }
        }

        return $return;

    }

    /**
     * Map data to component
     *
     * @param array $block_data
     * @param GF\Models\Component_PB_Block $component
     * @return Object/Array
     */
    public static function map_data_to_component($data, $component)
    {

        $clone = clone $component;
        $has_sub_components = array_has_items($clone->sub_components) ? true : false;

        //Loop trough data
        foreach ($data as $key => $value) {

            //This data belongs to a sub component
            if ($has_sub_components && isset($clone->sub_components[$key])) {

                //Is an array of sub components
                if (array_has_items($value)) {
                    // dpr($value, 2);
                    $clone->{$key} = array();

                    if (isset($component->dataTypes[$key]) && strpos($component->dataTypes[$key], 'array[') !== false) {
                        foreach ($value as $v) {
                            $clone->{$key}[] = self::map_data_to_component($v, $clone->sub_components[$key]);
                        }
                    } else {
                        $clone->{$key} = self::map_data_to_component($value, $clone->sub_components[$key]);
                    }

                } elseif ($value) {

                    $clone->{$key} = self::map_data_to_component($value, $clone->sub_components[$key]);
                }

            } elseif ($value) {

                //Deserialize it!
                $data = isset($component->dataTypes[$key]) ? deserializeData($value, $component->dataTypes[$key]) : $value;

                //Just set value
                $clone->{$key} = $data;
            }

        }

        return $clone;

    }

    /**
     * Map Custom CPT Models to
     * A Component
     *
     * @param array[\Custom_Cpt_Model] $models
     * @param \GF\Models\Component $component
     * @return array[\GF\Models\Component]
     */
    public static function map_models_to_component($models, $component)
    {

        $result = [];

        if (!array_has_items($models)) {
            return $result;
        }

        foreach ($models as $model) {
            $result[] = self::map_data_to_component($model->get_component_data(), $component);
        }

        return $result;

    }

    /**
     * Merge two arrays
     *
     * @param array $paArray1
     * @param array $paArray2
     * @return array
     */
    public static function merge($paArray1, $paArray2)
    {
        if (!is_array($paArray1) or !is_array($paArray2)) {
            return $paArray2;
        }
        foreach ($paArray2 as $sKey2 => $sValue2) {
            $paArray1[$sKey2] = self::merge(@$paArray1[$sKey2], $sValue2);
        }
        return $paArray1;
    }

}
