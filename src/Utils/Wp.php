<?php

if(!function_exists('wp_allow_svg_uploads')){

    function wp_allow_svg_uploads($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    add_filter('upload_mimes', 'wp_allow_svg_uploads');
    
}