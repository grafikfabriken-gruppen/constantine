<?php
namespace GF\Utils;

if (!class_exists('GF\\Utils\\Singleton')) {

    /**
     * Singleton Class
     * Prevent a class to iniate twict
     *
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    abstract class Singleton
    {
        /**
         * An collection of all instances
         *
         * @var array
         */
        private static $_instances = array();

        /**
         * _destruct function
         * On your classes create a function called _destruct
         *
         * @author Hampus Alstermo <hampus@grafikfabriken.nu>
         * @version 1.0
         */
        public function __destruct()
        {
            if (method_exists($this, '_destruct')) {
                call_user_func(array($this, '_destruct'));
            }
        }

        /**
         * The construct function
         *
         * On your classes create a function called _construct instead
         * @author Hampus Alstermo <hampus@grafikfabriken.nu>
         * @version 1.0
         */
        protected function __construct()
        {
            if (method_exists($this, '_construct')) {
                call_user_func(array($this, '_construct'));
            }
        }

        /**
         * Disable clone
         *
         * @return void
         * @author Hampus Alstermo <hampus@grafikfabriken.nu>
         * @version 1.0
         */
        protected function __clone()
        {
        }

        /**
         * Returns the instance 
         * of the class
         *
         * @return void
         * @author Hampus Alstermo <hampus@grafikfabriken.nu>
         * @version 1.0
         */
        public static function getInstance()
        {
            $class = get_called_class();
            if (!isset(self::$_instances[$class])) {
                self::$_instances[$class] = new $class();
            }
            return self::$_instances[$class];
        }

    }
}