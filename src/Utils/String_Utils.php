<?php
namespace GF\Utils;

if(!defined('ABSPATH')){
    exit;
}

class String_Utils{

    /**
     * Check if string is href
     *
     * @param string $str
     * @return boolean
     */
    public static function is_href($str)
    {
        return preg_match("@^https?://@", $str) ? true : false;
    }
}