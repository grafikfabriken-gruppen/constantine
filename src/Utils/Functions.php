<?php
namespace GF\Utils;

use GF\Controllers\Bootstrap_Controller;
use GF\Controllers\Manifest_Controller;
use GF\Controllers\Template_Controller;
use GF\Controllers\Template_Wrap_Controller;
use GF\Controllers\Page_Builder_Controller;
use GF\Controllers\Block_Controller;
use GF\Models\Builder_Block;
use GF\Factories\Builder_Block_Factory;
use GF\Factories\Fields_Factory;
use GF\Factories\Options_Page_Factory;
use GF\Controllers\Cache_Controller;

/**
 * 
 * Borrowed from the internet
 * @see https://stackoverflow.com/questions/3808602/recursively-replace-keys-in-an-array
 *
 * @param array $input
 * @return void
 */
function replace_keys(array $input, $search = '', $replace = '') {

    $return = array();
    foreach ($input as $key => $value) {

        $key = str_replace($search, $replace, $key);

        if (is_array($value)){
            $value = replace_keys($value, $search, $replace); 
        }

        $return[$key] = $value;
    }
    return $return;
}


/**
 * Bootstrap app
 *
 * @return GF\Controllers\Bootstrap_Controller
 */
function bootstrap(){
    return Bootstrap_Controller::getInstance();
}

/**
 * Manifest controller
 *
 * @return Manifest_Controller
 */
function manifestController(){
    return Manifest_Controller::getInstance();
}

/**
 * TemplateController
 *
 * @return Template_Controller
 */
function templateController(){
    return Template_Controller::getInstance();
}

/**
 * Template Wrap Controller
 *
 * @return Template_Wrap_Controller
 */
function templateWrapController(){
    return Template_Wrap_Controller::getInstance();
}

/**
 * Get pagebuilder
 *
 * @return Page_Builder_Controller
 */
function pageBuilder(){
    return Page_Builder_Controller::getInstance();
}

/**
 * Render a block
 *
 * @param Builder_Block $block
 * @return string
 */
function renderBlock($block)
{
    return pageBuilder()->render_block($block);
}

/**
 * Block controller
 *
 * @return Block_Controller
 */
function blockController(){
    return Block_Controller::getInstance();
}

/**
 * Builder block factory
 *
 * @return Builder_Block_Factory
 */
function builderBlockFactory(){
    return Builder_Block_Factory::getInstance();
}

/**
 * Fields factory
 *
 * @return Fields_Factory
 */
function fieldsFactory(){
    return Fields_Factory::getInstance();
}

/**
 * Options page factory
 *
 * @return Options_Page_Factory
 */
function optionsPageFactory(){
    return Options_Page_Factory::getInstance();
}

/**
 * Cache controller
 *
 * @return Cache_Controller
 */
function cacheController(){
    return Cache_Controller::getInstance();
}