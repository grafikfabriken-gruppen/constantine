<?php
namespace GF\Interfaces;

interface Interface_Component{


    /**
     * Before render component
     *
     * @return void
     */
    public function before_render();

    /**
     * Render component
     *
     * @return void
     */
    public function render();
    
    /**
     * This method runs when component is installed
     *
     * @return void
     */
    public function theme_hooks();

    /**
     * Hook for set custom options or data
     *
     * @return void
     */
    public function set_options();

    /**
     * Load acf fields
     *
     * @return void
     */
    public function load_fields();

    /**
     * Install Component in page builder
     *
     * @return void
     */
    public function install_to_page_builder();


    /**
     * Install sub components
     *
     * @return void
     */
    public function install_sub_components();


    
}