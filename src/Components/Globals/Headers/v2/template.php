<?php /** @var \GF\Components\Globals\Headers\v2\Component $this */ ?>
<header class="global-header js-header">
    <div class="container">
        <div class="gh-header">
            <a href="<?= home_url(); ?>">
                <img src="<?= $this->get_header_logo(); ?>" alt="logo" class="gh-header__logo">
            </a>
            <a href="#" class="btn js-navtoggle" role="button" data-label-close="<?= __('Close', 'grafikfabriken') ?>"
                data-label-menu="<?= __('Menu', 'grafikfabriken') ?>">
                <?= __('Menu', 'grafikfabriken') ?></a>
        </div>
        <!-- /.btn -->
        <!-- /.gh-header -->
        <nav class="gh-nav">
            <?= 
        wp_nav_menu(array(
              "menu" => $this->menu,
              "menu_id" => $this->menu,
              "theme_location" => $this->menu,
              "menu_class" => "list-unstyled text-center",
              "container" => "ul"
        ));
      ?>
            <!-- /.list-unstyled -->
        </nav>
        <!-- /.gh-nav -->
    </div>
    <!-- /.container -->
</header>
<!-- /.global-header -->