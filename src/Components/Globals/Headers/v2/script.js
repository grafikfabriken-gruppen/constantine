import $ from "jquery";

$(document).ready(() => {
    const navToggle = $(".js-navtoggle");
    const header = $(".js-header");
    const body = $("body");

    const scrollBarWidth = window.outerWidth - $(window).innerWidth();
    const hasScrollBar = window.outerWidth > $(window).innerWidth();

    const closeLabel = navToggle.data("label-close");
    const menuLabel = navToggle.data("label-menu");

    navToggle.on("click", () => {
        header.toggleClass("js-navopen");
        body.toggleClass("js-navopen");

        if (header.hasClass("js-navopen")) {
            navToggle.html(closeLabel);
        } else {
            navToggle.html(menuLabel);
        }

        if (header.hasClass("js-navopen") && hasScrollBar) {
            body.css("padding-right", scrollBarWidth);
        } else {
            body.removeAttr("style");
        }
    });
});
