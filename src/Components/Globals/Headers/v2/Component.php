<?php
namespace GF\Components\Globals\Headers\v2;

use function GF\Utils\optionsPageFactory;

final class Component extends \GF\Models\Component
{

    /**
     * Menu
     *
     * @var string
     */
    public $menu = "main_menu";

    /**
     * Construct component
     */
    public function __construct()
    {
        $this->has_options = true;
    }

    /**
     * Create a global settings page
     *
     * @return void
     */
    public function theme_hooks()
    {
        //Add options page
        optionsPageFactory()->create_theme_sub_page(__('Header', "grafikfabriken"), 'header-settings', $this->fields);

        //Register menu
        register_nav_menus(array(
            $this->menu => __('Main menu', "grafikfabriken"),
        ));

        //Change li class
        add_filter('nav_menu_css_class', array($this, 'custom_nav_menu_css_class'), 10, 3);

        //Change a class
        add_filter('nav_menu_link_attributes', array($this, 'add_specific_menu_location_atts'), 10, 3);

    }

    /**
     * Change li classes
     *
     * @param array $classes
     * @param stdClass $item
     * @param stdClass $args
     * @return array
     */
    public function custom_nav_menu_css_class($classes, $item, $args)
    {

        if ($args->menu === $this->menu) {
            $classes = ['nav-item'];
            if ($item->current) {
                $classes[] = 'active';
            }

            return $classes;
        }

        return $classes;

    }

    /**
     * Change link class
     *
     * @param array $classes
     * @param stdClass $item
     * @param stdClass $args
     * @return array
     */
    public function add_specific_menu_location_atts($classes, $item, $args)
    {

        if ($args->menu === $this->menu) {
            $classes["class"] = 'nav-link';
            return $classes;
        }

        return $classes;

    }

    public function get_header_logo()
    {

        $header_logo = get_field("header_logo", "options");
        return $header_logo ? wp_get_attachment_image_url($header_logo, 'full') : "";

    }

}
