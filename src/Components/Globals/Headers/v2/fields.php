<?php
return [
    array(
        'key' => 'header_logo',
        'label' => __('Logotype', "grafikfabriken"),
        'name' => 'header_logo',
        'type' => 'image',
        'return_format' => 'id',
        'preview_size' => 'thumbnail',

    ),
];
