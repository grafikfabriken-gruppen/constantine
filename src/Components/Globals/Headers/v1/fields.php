<?php
return [
    array(
        'key' => 'header_title',
        'label' => __('Title', "grafikfabriken"),
        'name' => 'header_title',
        'type' => 'text',
        'class_key' => 'title'
    )
];