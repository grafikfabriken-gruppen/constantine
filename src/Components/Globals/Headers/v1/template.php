<?php 
/** @var \GF\Components\Globals\Headers\v1\Component $this */
$this; ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
    <div class="container">
        <a class="navbar-brand" href="<?= home_url(); ?>"><?= $this->title; ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <?= wp_nav_menu(array(
            "menu" => $this->menu,
            "menu_id" => $this->menu,
            "theme_location" => $this->menu,
            "menu_class" => "navbar-nav",
            "container" => "ul"
        ));?>
        </div>
  </div>
</nav>