<?php

return [
    array(
        'key' => 'footer_title_one',
        'label' => __('Heading 1', "grafikfabriken"),
        'name' => 'footer_title_one',
        'type' => 'text',
        'class_key' => 'title_one'
    ),
    array(
        'key' => 'footer_content_one',
        'label' => __('Content 1', "grafikfabriken"),
        'name' => 'footer_content_one',
        'type' => 'wysiwyg',
        'class_key' => 'content_one'
    ),
    array(
        'key' => 'footer_title_two',
        'label' => __('Heading 2', "grafikfabriken"),
        'name' => 'footer_title_two',
        'type' => 'text',
        'class_key' => 'title_two'
    ),
    array(
        'key' => 'footer_content_two',
        'label' => __('Content 2', "grafikfabriken"),
        'name' => 'footer_content_two',
        'type' => 'wysiwyg',
        'class_key' => 'content_two'
    ),
    array(
        'key' => 'footer_copyright',
        'label' => __('Copyright text', "grafikfabriken"),
        'name' => 'footer_copyright',
        'type' => 'wysiwyg',
        'class_key' => 'copyright'
    )
];