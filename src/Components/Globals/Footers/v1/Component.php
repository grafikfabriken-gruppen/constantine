<?php
namespace GF\Components\Globals\Footers\v1;

use function GF\Utils\optionsPageFactory;


final class Component extends \GF\Models\Component{

    /**
     * Footer title
     *
     * @var string
     */
    public $title_one;

    /**
     * Content on
     *
     * @var string
     */
    public $content_one;

    /**
     * Title two
     *
     * @var string
     */
    public $title_two;

    /**
     * Content two
     *
     * @var string
     */
    public $content_two;

    /**
     * Copyright
     *
     * @var string
     */
    public $copyright;
    
    /**
     * Construct component
     */
    public function __construct()
    {
        $this->has_options = true;
        $this->cache = true;
    }

    /**
     * Create a global settings page
     *
     * @return void
     */
    public function theme_hooks()
    {
        optionsPageFactory()->create_theme_sub_page(__('Footer', 'grafikfabriken'), 'footer-settings', $this->fields, $this->id);
    }

}