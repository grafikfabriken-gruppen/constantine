<?php /** @var \GF\Components\Globals\Footers\v1\Component $this */ $this; ?>
<footer class="footer_v1">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h1><?= $this->title_one; ?></h1>
                <?= $this->content_one?>
            </div>
            <div class="col-12 col-sm-6">
                <h1><?= $this->title_two; ?></h1>
                <?= $this->content_two ?>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col text-center">
                <small><?= $this->copyright?></small>
            </div>
        </div>
    </div>
</footer>
