<?php
namespace GF\Components\Globals\Footers\v2;

use function GF\Utils\optionsPageFactory;
use \GF\Utils\Array_Utils;

final class Component extends \GF\Models\Component{

  
    /**
     * Copyright
     *
     * @var string
     */
    public $copyright;
    
    /**
     * Contact information partaial
     *
     * @var \GF\Components\Partials\Settings\contact_information\Component 
     */
    public $contact_information;


    public function __construct(){
        // $this->cache = true;

        $this->partials = [
            "contact_information" => array(
                "id" => "contact_information",
                "folder" => "settings"
            ),
        ];

        $this->dataTypes["contact_information"] = "array[\\GF\\Components\\Partials\\Settings\\contact_information\\Component]";
        
    }

    /**
     * Create a global settings page
     *
     * @return void
     */
    public function theme_hooks(){
        optionsPageFactory()->create_theme_sub_page(__('Footer', 'grafikfabriken'), 'footer-settings', $this->option_fields, $this->id);
        add_filter('contact-information-settings', array($this, 'setup_dependent_contact_fields'));
    }

    public function setup_dependent_contact_fields($fields){
        $fields[] = 'visiting_address_tab';
        $fields[] = 'visiting_address';
        return $fields;
    }


    /**
     * Returns the full html for the footer logo
     * @todo Make this from a partial and not direct theme option
     * @return string
     */
    public function get_footer_logo($width = 200){

        $footer_logo = get_field("footer_logo", "options");
        $footer_full_image = wp_get_attachment_image_src($footer_logo, 'full');

        $image_url = $footer_full_image[0];
        
        if (array_has_items($footer_full_image)) {

            $croped_image = aq_resize($image_url, $width);    
            // If the image is a .svg it wont return anything, therefore use only the croped image if it could be croped
            if($croped_image) $image_url = $croped_image;
        }
        
        return $image_url;
    }
    
    /**
     * Get the footer background url
     * @todo Make this from a partial and not direct theme option
     * @return string
     */
    public function get_footer_background(){

        $footer_background = get_field("footer_background", "options");
        $footer_full_image = wp_get_attachment_image_src($footer_background, 'full');

        $image_url = $footer_full_image[0];
        
        
        if (array_has_items($footer_full_image)) {

            $croped_image = aq_resize($image_url, 1920, null, false);    
            // If the image is a .svg it wont return anything, therefore use only the croped image if it could be croped
            if($croped_image) $image_url = $croped_image;
        }
        
        return $image_url;

    }

}