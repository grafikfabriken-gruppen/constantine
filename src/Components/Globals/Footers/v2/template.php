<?php /** @var \GF\Components\Globals\Footers\v2\Component $this */ ?>

<footer style="background-image: url(<?= $this->get_footer_background(); ?>)" class="mt-6">
    <div class="container">
        <div class="row lg-gutters text-white pt-4">
            <div class="col-12 col-md-auto">
                <img src="<?= $this->get_footer_logo(); ?>" alt="" class="mb-4" />
            </div>
            <!-- /.col -->
            <div class="col-6 col-md-auto">
                <small>
                    <?= __('Visiting address', 'grafikfabriken'); ?></small>
                <ul class="list-unstyled mb-4">
                    <li>
                        <?= $this->contact_information->visiting_address ?>
                    </li>
                    <li>
                        <?= $this->contact_information->visiting_postalcode . ' ' . $this->contact_information->visiting_city ?>
                    </li>
                </ul>
            </div>
            <!-- /.col -->
            <div class="col-6 col-md-auto">
                <small>
                    <?= __('Contact', 'grafikfabriken'); ?></small>
                <ul class="list-unstyled mb-4">
                    <li>tel:<a href="tel:<?= $this->contact_information->phone ?>">
                            <?= $this->contact_information->phone ?></a></li>
                    <li><a href="mailto:<?= $this->contact_information->email ?>">
                            <?= $this->contact_information->email ?></a></li>
                </ul>
            </div>
            <!-- /.col -->
        </div>

        <div class="row">
            <div class="col text-center text-uppercase">
                <small class="copyright">
                    <?= __("Production", "grafikfabriken"); ?>:
                    <a href="https://grafikfabriken.nu">Grafikfabriken</a></small>
            </div>
            <!-- /.col text-center -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</footer>