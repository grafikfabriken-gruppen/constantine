<?php

return [
    array(
        'key' => 'footer_logo',
        'label' => __('Logotype', "grafikfabriken"),
        'name' => 'footer_logo',
        'type' => 'image',
        'return_format' => 'id',
        'preview_size' => 'thumbnail',

    ),
    array(
        'key' => 'footer_background',
        'label' => __('Background', "grafikfabriken"),
        'name' => 'footer_background',
        'type' => 'image',
        'return_format' => 'id',
        'preview_size' => 'thumbnail',
        'required' => 1

    ),   
];