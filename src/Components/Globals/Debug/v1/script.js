import $ from 'jquery'

class CssRootVariables {
	constructor() {
		this.styleSheets = document.styleSheets
		this.bodyFontSize = $('body', 'html').css('font-size')
		this.cssVariables = []
		this.getRootVariables()
	}

	getRootVariables() {
		for (let i = 0; i < this.styleSheets.length; i++) {
			const declarations = this.styleSheets[i].rules || this.styleSheets[i].cssRules
			for (let i = 0; i < declarations.length; i++) {
				if (declarations[i].selectorText == ':root') {
					const rootDeclaration = declarations[i].cssText
						.replace(':root {', '')
						.replace('}')
						.split(';')

					for (let i = 0; i < rootDeclaration.length; i++) {
						const cssVariable = rootDeclaration[i]
						if (cssVariable && cssVariable != ' ') {
							this.cssVariables.push(cssVariable)
						}
					}
				}
			}
		}
	}
}

class Spacers extends CssRootVariables {
	constructor() {
		super()
		this.filterVarsForSpacers = this.cssVariables.filter(name => name.includes('--spacing'))
		this.splitArray = 0
		this.varName = 0
		this.varUnit = 0
		this.varUnitPx = 0
		this.makeSpacers()
	}
	makeSpacers() {
		this.filterVarsForSpacers.forEach(spacing => {
			this.splitArray = spacing.split(':')
			this.varName = this.splitArray[0].trim().slice(2)
			this.varUnit = this.splitArray[1].trim()
			this.varUnitPx = parseFloat(parseFloat(this.varUnit, 10) * parseFloat(this.bodyFontSize, 10))

			$('.js-spacers').append(
				`<div>
				<span><b>${this.varName}:</b></span>
				<span>${this.varUnit} (${this.varUnitPx}px)</span>
				</div>`,
			)
		})
	}
}

class ColorSwatch extends CssRootVariables {
	constructor() {
		super()

		this.filterVarsForColors = this.cssVariables.filter(name => name.includes('#'))
		this.splitArray = 0
		this.colorName = 0
		this.colorHex = 0
		this.setTextColor = 0

		this.makeColorSwatch()
	}

	getContrastYIQ(hexcolor) {
		this.r = parseInt(hexcolor.substr(0, 2), 16)
		this.g = parseInt(hexcolor.substr(2, 2), 16)
		this.b = parseInt(hexcolor.substr(4, 2), 16)
		this.yiq = (this.r * 299 + this.g * 587 + this.b * 114) / 1000
		return this.yiq >= 128 ? '#2f3640' : '#f5f6fa'
	}

	hex6dig(hex) {
		let chars = hex.split('')
		let val = ''
		for (let i = 0; i < chars.length; i++) {
			val += chars[i] + chars[i]
		}
		return val
	}

	makeColorSwatch() {
		this.filterVarsForColors.forEach(color => {
			this.splitArray = color.split(':')
			this.colorName = this.splitArray[0].trim().slice(2)
			this.colorHex = this.splitArray[1].trim().slice(1)

			if (this.colorHex.length < 4) {
				this.colorHex = this.hex6dig(this.colorHex)
			}

			this.setTextColor = this.getContrastYIQ(this.colorHex)

			if (this.colorName == 'primary') {
				$('.js-colorswatch').append(
					`
					<h6 class="mt-4">Theme Colors</h6>
					<hr>
					<div class="color-swatch" style="background:#${this.colorHex}; color: ${this.setTextColor}">
						<span>$${this.colorName}</span>
						<span>#${this.colorHex}</span>
					</div>
					`,
				)
			} else {
				$('.js-colorswatch').append(
					`
					<div class="color-swatch" style="background:#${this.colorHex}; color: ${this.setTextColor}">
						<span>$${this.colorName}</span>
						<span>#${this.colorHex}</span>
					</div>
					`,
				)
			}
		})
	}
}

class TypographyInfo {
	constructor($element) {
		this.textElement = $element.children()
		this.textOutput = $element.siblings('.js-textoutput')

		this.bodyFontSize = $('body', 'html').css('font-size')
		this.htmlTag = this.textElement.prop('tagName')
		this.className = this.textElement.prop('className')
		this.fontSize = this.textElement.css('font-size')
		this.lineHeight = this.textElement.css('line-Height')
		this.fontWeight = this.textElement.css('font-weight')
		this.marginBottom = this.textElement.css('margin-bottom')
		this.fontFamily = this.textElement.css('font-family')

		this.fontSizeRem = parseFloat(parseFloat(this.fontSize, 10) / parseFloat(this.bodyFontSize, 10))
		this.lineHeightUnit = parseFloat(parseFloat(this.lineHeight, 10) / parseFloat(this.fontSize, 10)).toFixed(1)
		this.marginBottomRem = parseFloat(parseFloat(this.marginBottom, 10) / parseFloat(this.bodyFontSize, 10))

		this.setText()
	}

	setText() {
		if (this.className.length > 0) {
			this.textOutput.html(
				`<span>.${this.className}</span>
				<span><b>fs:</b> ${this.fontSizeRem}rem(${this.fontSize})</span>
				<span><b>lh:</b> ${this.lineHeightUnit}</span>
				<span><b>fw:</b> ${this.fontWeight}</span>
				<span><b>mb:</b> ${this.marginBottomRem}rem(${this.marginBottom})</span>
				<span><b>ff:</b> ${this.fontFamily}</span>`,
			)
		} else {
			this.textOutput.html(
				`<span>${this.htmlTag}</span>
				<span><b>fs:</b> ${this.fontSizeRem}rem(${this.fontSize})</span>
				<span><b>lh:</b> ${this.lineHeightUnit}</span>
				<span><b>fw:</b> ${this.fontWeight}</span>
				<span><b>mb:</b> ${this.marginBottomRem}rem(${this.marginBottom})</span>
				<span><b>ff:</b> ${this.fontFamily}</span>`,
			)
		}
	}
}

class Debug {
	constructor() {
		this.clickElement = $('#debug-toggle')
		this.html = $('html')

		this.clickEvent()
		this.html.toggleClass(window.localStorage.debuggToggled)
	}

	clickEvent() {
		this.clickElement.on('click', () => {
			if (window.localStorage.debuggToggled != 'debug') {
				this.html.toggleClass('debug', true)
				window.localStorage.debuggToggled = 'debug'
			} else {
				this.html.toggleClass('debug', false)
				window.localStorage.debuggToggled = ''
			}
		})
	}
}

$(document).ready(function() {

	try {

		const debugg = new Debug()
		const spacers = new Spacers()
		const colorSwatch = new ColorSwatch()

		$('.typography')
			.find('.js-textelement')
			.each(function () {
				const typographyInfo = new TypographyInfo($(this))
			})
		
	} catch (e) {
		console.error("Could not boot Debug section...");
	}
	
})
