<div class="debug_template_v1">
	<div class="container">
		<div class="row">

			<div class="col mb-4">
				<h2>Styleguide</h2>
			</div><!-- /.col -->


			<!-- SPACERS -->
			<div class="container spacers-container">
				<div class="row">
					<div class="col-sm-3">
						<span>Spacers</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9 js-spacers">
					</div><!-- /.col-sm-9 js-spacers -->
				</div><!-- /.row -->
			</div><!-- /.container spacers-container -->
			<!-- SPACERS ENDS -->

			<!-- COLORS -->
			<div class="container color-container">
				<div class="row">
					<div class="col-sm-3">
						<span>Colors</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9 js-colorswatch">
					</div><!-- /.col-sm-9 js-colorelement -->
				</div><!-- /.row -->
			</div><!-- /.container color-container -->
			<!-- COLORS ENDS -->

			<!-- TYPOGRAPHY -->
			<div class="container typography-container">
				<!-- typography starts -->
				<!-- headings -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h1>Lorem, ipsum dolor.</h1>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h2>Lorem, ipsum dolor.</h2>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h3>Lorem, ipsum dolor.</h3>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h4>Lorem, ipsum dolor.</h4>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">
					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h5>Lorem, ipsum dolor.</h5>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h6>Lorem, ipsum dolor.</h6>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<!-- headings ends -->

				<div class="row typography">
					<div class="col-sm-3 js-textoutput">
					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<a href="#">Lorem ipsum dolor sit amet.</a>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->

				<div class="row typography">
					<div class="col-sm-3 js-textoutput">
					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid accusamus sapiente iusto
							natus
							recusandae. Alias illum quibusdam earum error minima tempora voluptas? Cum in ullam,
							inventore
							nulla maiores atque laborum!</p>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">
					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<strong>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eaque illo illum reprehenderit
							quidem cum, dolorem numquam! Iusto nemo, ipsam reprehenderit debitis, suscipit placeat odio
							ducimus sint repellendus porro pariatur voluptatibus?</strong>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">
					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus consectetur
							beatae
							voluptatibus nemo sed? Quo qui necessitatibus reiciendis fuga omnis? Labore, porro ipsum nam
							architecto reprehenderit eius consequatur excepturi vero!</small>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->


				<!-- displays -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h1 class="display-1">Lorem, ipsum dolor.</h1>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h1 class="display-2">Lorem, ipsum dolor.</h1>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h1 class="display-3">Lorem, ipsum dolor.</h1>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">

					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<h1 class="display-4">Lorem, ipsum dolor.</h1>
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<!-- displays ends-->

				<!-- classes -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">
					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis nulla
							asperiores ea architecto impedit quam distinctio veniam porro non labore nisi similique
							dolores
							totam culpa vero sequi nam, pariatur iste?</p><!-- /.lead -->
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<div class="row typography">
					<div class="col-sm-3 js-textoutput">
					</div><!-- /.col-sm-3 js-textoutput -->
					<div class="col-sm-9 js-textelement">
						<p class="small">Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis nulla
							asperiores ea architecto impedit quam distinctio veniam porro non labore nisi similique
							dolores
							totam culpa vero sequi nam, pariatur iste?</p><!-- /.lead -->
					</div><!-- /.col-sm-9 js-textelement -->
				</div><!-- /.row typography -->
				<!-- classes ends -->
				<!-- typography ends -->
			</div><!-- /.container typography-container -->
			<!-- TYPOGRAPHY ENDS -->

			<!-- BUTTONS -->
			<div class="container buttons-container">
				<div class="row typography">
					<div class="col-sm-3">
						<span>.btn .btn-primary</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<button class="btn btn-sm btn-primary">button</button><!-- /.btn btn-sm btn-primary -->
						<button class="btn btn-primary">button</button><!-- /.btn btn-primary -->
						<button class="btn btn-lg btn-primary">button</button><!-- /.btn btn-lg btn-primary -->
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>.btn .btn-outline-primary</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<button class="btn btn-sm btn-outline-primary">button</button>
						<!-- /.btn btn-sm btn-outline-primary-->
						<button class="btn btn-outline-primary">button</button><!-- /.btn btn-outline-primary -->
						<button class="btn btn-lg btn-outline-primary">button</button>
						<!-- /.btn btn-lg btn-outline-primary -->
					</div><!-- /.col-sm-9 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>.btn .btn-link</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<button class="btn btn-sm btn-link">button</button><!-- /.btn btn-sm btn-link-->
						<button class="btn btn-link">button</button><!-- /.btn btn-link -->
						<button class="btn btn-lg btn-link">button</button><!-- /.btn btn-lg btn-link -->
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->
			</div><!-- /.container buttons-container -->
			<!-- BUTTONS ENDS -->

			<!-- FORMS -->
			<div class="container form-container">

				<div class="row">
					<div class="col-sm-3">
						<span>Form input</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group">
								<label for="exampleName1">Name</label>
								<input type="text" class="form-control" id="exampleName1" placeholder="Name">
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Form disabled input</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group">
								<label for="exampleName2">Name</label>
								<input type="text" class="form-control" id="exampleName2" placeholder="Name" disabled>
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Form textarea</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group">
								<label for="exampleTextarea">Textarea</label>
								<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Form select</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group">
								<label for="exampleSelect">Select</label>
								<select class="form-control selectpicker" id="exampleSelect" data-style="">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Form select disabled</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group">
								<label for="exampleSelectDisabled">Select Disabled</label>
								<select class="form-control selectpicker" id="exampleSelectDisabled" data-style=""
									disabled>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Form search input</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group ">
								<label for="exampleSearch">Search</label>
								<input type="search" class="form-control" id="exampleSearch" placeholder="Search">
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Form .row</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="exampleName3">Name</label>
										<input type="text" class="form-control" id="exampleName3" placeholder="Name">
									</div><!-- /.form-group -->
								</div><!-- /.col -->
								<div class="col">
									<div class="form-group">
										<label for="exampleLastname1">Lastname</label>
										<input type="text" class="form-control" id="exampleLastname1"
											placeholder="Name">
									</div><!-- /.form-group -->
								</div><!-- /.col -->
							</div><!-- /.row -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Form .form-row</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-row">
								<div class="col">
									<div class="form-group">
										<label for="exampleName5">Name</label>
										<input type="text" class="form-control" id="exampleName5" placeholder="Name">
									</div><!-- /.form-group -->
								</div><!-- /.col -->
								<div class="col">
									<div class="form-group">
										<label for="exampleLastname2">Lastname</label>
										<input type="text" class="form-control" id="exampleLastname2"
											placeholder="Name">
									</div><!-- /.form-group -->
								</div><!-- /.col -->
							</div><!-- /.form-row -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Custom radio</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group">
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio1" name="customRadio"
										class="custom-control-input" checked>
									<label class="custom-control-label" for="customRadio1">Custom radio</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio2" name="customRadio"
										class="custom-control-input">
									<label class="custom-control-label" for="customRadio2">Custom radio</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio3" name="customRadio"
										class="custom-control-input" disabled>
									<label class="custom-control-label" for="customRadio3">Custom radio disabled</label>
								</div>
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Custom checkbox</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customCheck1" checked>
									<label class="custom-control-label" for="customCheck1">Custom checkbox</label>
								</div><!-- /.custom-control custom-checkbox -->
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customCheck2">
									<label class="custom-control-label" for="customCheck2">Custom checkbox</label>
								</div><!-- /.custom-control custom-checkbox -->
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customCheck3" disabled>
									<label class="custom-control-label" for="customCheck3">Custom checkbox
										disabled</label>
								</div><!-- /.custom-control custom-checkbox -->
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Custom switch</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group">
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="customSwitch1" checked>
									<label class="custom-control-label" for="customSwitch1">Toggle this switch
										element</label>
								</div>
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="customSwitch2">
									<label class="custom-control-label" for="customSwitch2">Toggle this switch
										element</label>
								</div>
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" disabled id="customSwitch3">
									<label class="custom-control-label" for="customSwitch3">Disabled switch
										element</label>
								</div>
							</div><!-- /.form-group -->
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

				<div class="row">
					<div class="col-sm-3">
						<span>Form validation</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">
							<div class="form-group is-valid">
								<label for="exampleValid">Name</label>
								<input type="text" class="form-control is-valid" id="exampleValid" placeholder="Name">
								<div class="valid-feedback">Looks good!</div><!-- /.valid-feedback -->
							</div><!-- /.form-group -->

							<div class="form-group is-invalid">
								<label for="exampleInvalid">Name</label>
								<input type="text" class="form-control is-invalid" id="exampleInvalid"
									placeholder="Name">
								<div class="invalid-feedback">Please enter a name.</div><!-- /.invalid-feedback -->
							</div><!-- /.form-group -->


							<div class="form-group">
								<div class="custom-control custom-checkbox is-valid">
									<input type="checkbox" class="custom-control-input is-valid" id="customValid">
									<label class="custom-control-label" for="customValid">Custom checkbox valid</label>
									<div class="valid-feedback">Looks good!</div><!-- /.valid-feedback -->
								</div><!-- /.custom-control custom-checkbox -->

								<div class="custom-control custom-checkbox is-invalid">
									<input type="checkbox" class="custom-control-input is-invalid" id="customInvalid">
									<label class="custom-control-label" for="customInvalid">Custom checkbox
										invalid</label>
									<div class="invalid-feedback">Looks bad!</div><!-- /.invalid-feedback -->
								</div><!-- /.custom-control custom-checkbox -->
							</div><!-- /.form-group -->

							<div class="col-12 form-feedback-valid">
								<h6>Valid</h6>
								<span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum numquam repellat
									eos
									recusandae dolorem soluta voluptate porro sint consectetur maxime?</span>
							</div><!-- /.col-12 -->

							<div class="col-12 form-feedback-invalid">
								<h6>Invalid</h6>
								<span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, obcaecati!</span>
							</div><!-- /.col-12 -->

						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->


				<div class="row">
					<div class="col-sm-3">
						<span>Test Form</span>
					</div><!-- /.col-sm-3 -->
					<div class="col-sm-9">
						<form name="styleguide">

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="testExampleName">Name</label>
										<input type="text" class="form-control" id="TestexampleName" placeholder="Name">
									</div><!-- /.form-group -->
								</div><!-- /.col-sm-6 -->
								<div class="col-sm-6">
									<div class="form-group">
										<label for="TestexampleLastname">Lastname</label>
										<input type="text" class="form-control" id="testExampleLastname"
											placeholder="Lastname">
									</div><!-- /.form-group -->
								</div><!-- /.col-sm-6 -->
							</div><!-- /.row -->

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="testExampleEmail">E-mail</label>
										<input type="email" class="form-control" id="testExampleEmail"
											placeholder="E-mail">
									</div><!-- /.form-group -->
								</div><!-- /.col-sm-6 -->
								<div class="col-sm-6">
									<div class="form-group">
										<label for="testExamplePhone">Phone number</label>
										<input type="tel" class="form-control" id="testExamplePhone"
											placeholder="Phone number">
									</div><!-- /.form-group -->
								</div><!-- /.col-sm-6 -->
							</div><!-- /.row -->

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="testExampleCountry">Country</label>
										<select class="form-control selectpicker" id="testExampleCountry" data-style="">
											<option data-hidden="true" value="" disable>Choose a country...</option>
											<option>Angola</option>
											<option>Bolivia</option>
											<option>Czech Republic</option>
											<option>Guatemala</option>
											<option>New Zealand</option>
										</select>
									</div><!-- /.form-group -->
								</div><!-- /.col-sm-6 -->
								<div class="col-sm-6">
									<div class="form-group">
										<label for="testExampleRegion">Region</label>
										<select class="form-control selectpicker" id="testExampleRegion" data-style="">
											<option data-hidden="true" value="" disable>Choose a country...</option>
											<option>Mauritius</option>
											<option>Poland</option>
											<option>Czech Republic</option>
											<option>Romania</option>
											<option>New Zealand</option>
											<option>Saudi Arabia</option>
										</select>
									</div><!-- /.form-group -->
								</div><!-- /.col-sm-6 -->
							</div><!-- /.row -->

							<div class="form-group">
								<label for="testExamplePassword">Password</label>
								<input type="password" class="form-control" id="testExamplePassword"
									placeholder="Password">
							</div><!-- /.form-group -->

							<div class="form-group">
								<label for="testExampleRepeatPassword">Repeat password</label>
								<input type="password" class="form-control" id="testExampleRepeatPassword"
									placeholder="Repeat password">
							</div><!-- /.form-group -->

							<div class="form-group">
								<label for="">Gender Inline</label>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioGenderMaleInline" name="customRadioGenderInline"
										class="custom-control-input" checked>
									<label class="custom-control-label" for="customRadioGenderMaleInline">Male</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioGenderFemaleInline"
										name="customRadioGenderInline" class="custom-control-input">
									<label class="custom-control-label"
										for="customRadioGenderFemaleInline">Female</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioGenderCatInline" name="customRadioGenderInline"
										class="custom-control-input">
									<label class="custom-control-label" for="customRadioGenderCatInline">Cat</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioGenderOtherInline" name="customRadioGenderInline"
										class="custom-control-input">
									<label class="custom-control-label" for="customRadioGenderOtherInline">Other</label>
								</div>
							</div><!-- /.form-group -->

							<div class="form-group">
								<label for="">Gender</label>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadioGenderMale" name="customRadioGender"
										class="custom-control-input" checked>
									<label class="custom-control-label" for="customRadioGenderMale">Male</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadioGenderFemale" name="customRadioGender"
										class="custom-control-input">
									<label class="custom-control-label" for="customRadioGenderFemale">Female</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadioGenderOther" name="customRadioGender"
										class="custom-control-input">
									<label class="custom-control-label" for="customRadioGenderOther">Other</label>
								</div>
							</div><!-- /.form-group -->

							<div class="form-group">
								<label for="testExampleTextarea">Message</label>
								<textarea class="form-control" id="testExampleTextarea" rows="4"></textarea>
							</div><!-- /.form-group -->

							<div class="form-group">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customTerms">
									<label class="custom-control-label" for="customTerms">I agree to terms</label>
								</div><!-- /.custom-control custom-checkbox -->
							</div><!-- /.form-group -->

							<button class="btn btn-primary" type="submit">Submit form</button>
						</form>
					</div><!-- /.col-sm-9 -->
				</div><!-- /.row -->

			</div><!-- /.container form-container -->
			<!-- FORMS ENDS -->

			<!-- DEBUGG BUTTON -->
			<div id="debug-toggle" class="debug-toggle js-debug btn btn-primary btn-lg">
				Debug
			</div><!-- /.debug-toggle -->
			<!-- DEBUGG BUTTON ENDS -->

		</div><!-- /.row -->
	</div><!-- .container -->
</div><!-- .debug_template_v1 -->