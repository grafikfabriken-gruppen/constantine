<?php
namespace GF\Components\Globals\Debug\v1;

final class Component extends \GF\Models\Component
{

	public $css;

	public function __construct()
	{
		$this->id = 'debug_information';
		$this->css = 'debug_template_v1';
	}

	public function debug(){
		// dpr($_SERVER);
	}

	public function render()
	{
		$is_dev = function_exists('is_dev_env') ? is_dev_env() : false;
		return $is_dev ? parent::render() : '';
	}
}