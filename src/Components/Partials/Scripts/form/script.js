import $ from 'jquery';
import { GF_Utils } from '../utils/script';
/**
 * Creates a form that
 */

class GF_Form {
    constructor(elm, args) {
        this.form = elm;
        this.btn = elm.find('.gf-form-submit');
        this.plups = [];
        this.args = args;
        this.action = this.form.attr('action');
        this.method = this.form.attr('method');

        // Set default (empty) callbacks
        this._successCallback = data => {};
        this._errorCallBack = data => {};

        // The response message (success)
        this.responseMessage = this.form.find('.js-form-response-message');

        // If there's any errors, then we don't want to run this script
        if (this.hasErrors()) return;

        $(window).on('resize', this.windowResize.bind(this));
        $(window).trigger('resize');

        // When the form is submitted
        this.btn.on('click', this.submitForm.bind(this));

        //Ensure plupload is loaded!
        if (typeof plupload !== 'undefined') {
            this.initplup();
        }
    }

    hasErrors() {
        // Markup errors etc
        let hasErrros = false;

        if (this.btn.length == 0) {
            console.error('gfForm missing submit button (.gf-form-submit)');
            hasErrros = true;
        }

        if (!this.action) {
            console.error('gfForm missing action (<form action="my_action">)');
            hasErrros = true;
        }
        if (!this.method) {
            console.error('gfForm missing method (<form method="POST">)');
            hasErrros = true;
        }

        return hasErrros;
    }

    // Sets callbacks for the form
    setSuccessCallback(cb) {
        this._successCallback = cb;
    }

    setErrorCallback(cb) {
        this._errorCallBack = cb;
    }

    /**
     * Inits plupload if defined
     * @returns {undefined}
     */
    initplup() {
        this.form.find('input[plupload="true"]').each(function() {
            var input = $(this);

            input.bind('focus keydown', function(event) {
                event.preventDefault();
                event.stopImmediatePropagation();
                input.trigger('click');

                return false;
            });

            var id = $(this).attr('id');
            var plup = new plupload.Uploader({
                browse_button: id,
                multipart: true,
                multi_selection: false,
                multipart_params: {
                    id: id,
                    action: 'plupload_file',
                    nonce: gf_login_plupload.nonce,
                },
                url: gf_app_js.ajax_url,
                filters: [{ title: 'Image files', extensions: 'jpeg,jpg,gif,png' }],
            });

            plup.input = input;

            plup.init();
            plup.bind('FilesAdded', this.FilesAdded);
            plup.bind('UploadProgress', this.UploadProgress);
            plup.bind('FileUploaded', this.FileUploaded);

            var span = $('<span/>', {
                class: 'loader',
                html: '<span class="fill"></span><div class="imageholder"></div>',
                style: 'width:' + input.outerWidth() + 'px',
            }).insertAfter(input);

            this.plups.push(plup);
        });
    }

    /**
     * WindowResize
     * @param {type} event
     * @returns {undefined}
     */
    windowResize(event) {
        var w = this.form
            .find('.loader')
            .parents('div')
            .find('input[plupload="true"]')
            .outerWidth();
        this.form.find('.loader').css({ width: w + 'px' });
        var ww = $(window).outerWidth();
        if (ww > 769) {
            this.form.find('input[data-type="date"]').each(function() {
                $(this).attr('type', 'text');
            });
        } else {
            this.form.find('input[data-type="date"]').each(function() {
                $(this).attr('type', 'date');
            });
        }
    }

    /**
     * File Uploaded
     * @param {type} up
     * @param {type} file
     * @param {type} ret
     * @returns {undefined}
     */
    FileUploaded(up, file, ret) {
        var uploader = this;
        var data = JSON.parse(ret.response);

        if (data.success) {
            $('<img/>', {
                src: data.data.url,
            }).appendTo(uploader.input.parent('div').find('span div.imageholder'));
            uploader.input.attr('data-value', data.data.id);
        }

        uploader.input
            .parent('div')
            .find('span.fill')
            .css({ 'background-color': '#19de00' });
        uploader.input.validate();
        this.removeBtnLoading();
    }

    /**
     * Upload progress
     * @param {type} up
     * @param {type} file
     * @returns {undefined}
     */
    UploadProgress(up, file) {
        var uploader = this;
        uploader.input
            .parent('div')
            .find('span.fill')
            .css({ width: file.percent + '%' });
    }

    /**
     * Files Added
     * @param {type} up
     * @param {type} files
     * @returns {undefined}
     */
    FilesAdded(up, files) {
        var uploader = this;
        uploader.input.attr('value', files[0]['name']);
        this.setBtnLoading();
        uploader.input
            .parent('div')
            .find('span.fill')
            .css({ width: '0px', 'background-color': '#009cde' });
        uploader.input
            .parent('div')
            .find('div.imageholder')
            .html('');
        up.start();
    }

    setBtnLoading() {
        this.btn.addClass('loading');
        this.btn.prop('disabled', true);
    }

    removeBtnLoading() {
        this.btn.removeClass('loading');
        this.btn.prop('disabled', false);
    }

    resetForm() {
        this.form.trigger('reset');
        this.form.find('.has-success').removeClass('has-success');
    }

    /**
     * submitingForm
     * @param {Eveent} event
     * @returns {Boolean}
     */
    submitForm(event) {
        event.preventDefault();
        event.stopPropagation();

        if (this.btn.hasClass('loading')) return false;

        // Validate the inputs that need to be required
        let valid = this.validateForm(this.form);

        //Check ReCaptcha
        if (this.form.find('.g-recaptcha').length > 0) {
            if (
                this.form.find('.g-recaptcha-response').length !== 0 &&
                this.form.find('.g-recaptcha-response').val() === ''
            ) {
                this.form.find('.g-recaptcha').addClass('error');
                return false;
            } else {
                this.form.find('.g-recaptcha').removeClass('error');
            }
        }

        if (!valid) {
            this.form.trigger('has_errors');
            return;
        }

        let data = this.form.serialize() + '&action=' + this.action;

        this.setBtnLoading();
        $.ajax({
            url: gf_app_js.ajax_url,
            type: this.method,
            data,
            success: this.ajax_response.bind(this),
            error: this.ajax_response_failed.bind(this),
        });
    }

    /**
     * Validates the inputs in the form
     * @param {jquery(elm)} form
     * @returns {bool}
     */
    validateForm(form) {
        let valid = true;

        form.find('input, .g-recaptcha-response, textarea, select, div[data-checkbox-group]').each(
            function() {
                let input = $(this);

                if (
                    input.attr('data-validate') == 'true' ||
                    input.attr('data-required') == 'true'
                ) {
                    let validate = input.validate();

                    if (!validate.isValid()) valid = false;
                }
            },
        );
        return valid;
    }

    ajax_response_failed(jqXHR, textStatus, error) {
        this._errorCallBack(error);
        this.removeBtnLoading();
    }

    /**
     * Ajax Response
     * @param {JSON} data
     * @param {type} textStatus
     * @param {type} jqXHR
     * @returns {Boolean}
     */
    ajax_response(data, textStatus, jqXHR) {
        if (typeof data === 'object') {
            if (data.success) {
                this.removeBtnLoading();

                // Clear all previous values
                this.form
                    .find('input, select, textarea')
                    .val('')
                    .prop('checked', false);

                // Show the success message
                this.responseMessage.slideDown();

                // Refresh the bootstrap selects when clearing the form
                $('.selectpicker').selectpicker('refresh');

                this._successCallback(data.data, self);

                //Reset Captcha
                if (this.form.find('.g-recaptcha').length > 0) {
                    grecaptcha.reset();
                }
                return true;
            }
        }

        this._errorCallBack(data);
        this.removeBtnLoading();
        return false;
    }

    static getInstance(args) {
        return new GF_Form(this, args);
    }
}

$.fn.gfForm = GF_Form.getInstance;
