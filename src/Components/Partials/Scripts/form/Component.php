<?php
namespace GF\Components\Partials\Scripts\form;

final class Component extends \GF\Models\Component{


    public function __construct()
    {
        $this->partials = [
            "validate" => array(
                "id" => "validate",
                "folder" => "scripts"
            )
        ];
    }

    public function theme_hooks()
    {
        $script_name = 'gf_app_js';
        add_filter('localize_script_' . $script_name, array($this, 'localize_script'));
        
    }

    public function localize_script($localize){
        $localize['form_i18n'] = array(
            'error' => __('Something went wrong', 'grafikfabriken')
        );        

        return $localize;
    }
}