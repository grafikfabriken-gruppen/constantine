<?php
namespace GF\Components\Partials\Scripts\validate;

final class Component extends \GF\Models\Component{

    public function __construct()
    {
        $this->partials = [
            "utils" => array(
                "id" => "utils",
                "folder" => "scripts"
            )
        ];
    }

    public function theme_hooks()
    {
        $script_name = 'gf_app_js';
        add_filter('localize_script_' . $script_name, array($this, 'localize_script'));
    }

    public function localize_script($localize){
        $localize['validate_i18n'] = array(
            'checkbox_group_error' => __('You must choose an option', 'grafikfabriken'),
            'required_field' => __('This field is required', 'grafikfabriken'),
            'not_valid_email' => __('You need to enter a valid email', 'grafikfabriken')
        );

        return $localize;
    }
}