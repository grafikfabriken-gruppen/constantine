import $ from 'jquery';
import GF_Utils from "../utils/script";
/**
 * jquery.validate.js
 * @author Hampus | Alexander
 * @version 1.0
 * @example var result = false;
*     $(".validate").each(function(){
*          result = $(this).validate();
*      });
* @requires jQuery
*/

class GF_Validate{

    constructor( input, force ){
        this.input = input;

        //Force validation
        if(typeof force === 'undefined'){
            force = this.input.hasClass("dep") ? true : false;
        }

        this.translate = gf_app_js.validate_i18n;
        
        // Set as standard
        this.dependent = false;
        
        this.type = this.input.prop('tagName').toLowerCase();
        // @todo Check if this is correct
        this.error_message = this.input.attr('data-error-message') ? this.input.attr('data-error-message') : this.translate.required_field;

        this.input_name = this.input.attr('name') ? this.input.attr('name') : '' ;

        this.response = {
            "msg": "",
            "type": "error",
            "success": false,
            "input": this.input,
            "name": this.input_name,
            "valid": true
        };


        // What kind of validation shall apply?
        switch (this.type) {
            case 'div':
                if (this.input.attr('data-checkbox-group')) {
                    this.validateCheckboxGroup();
                } else{
                    this.validateTextInput();
                }
                break;
            default:
                this.validateTextInput();
        }


    }

    /**
     * Validates a group of checkboxes (or radios) to see if the at least one value has been chosen
     * @todo Create more options such as min/max numbers of values etc
     */
    validateCheckboxGroup(){
        let group_name = this.input.attr('data-checkbox-group');
        let checkboxes = this.input.find('input[name="' + group_name+ '"]');

        let valid = false, _valid;
        checkboxes.each((index, checkbox) => {
            _valid = $(checkbox).prop('checked');
            
            if(_valid) valid = true;
        });
        

        if(valid){
            this.response.success = true;
        } else {
            this.response.msg = this.translate.checkbox_group_error;
        }

        
        this.afterValidation();

    }


    validateTextInput(){
        this.validate_as = this.input.attr("data-validate-as") ? this.input.attr("data-validate-as") : this.input.attr('type');

        
        // this.val = this.input.attr("value");
        this.val = this.input.val();

        this.req = this.input.attr('data-required') ? true : false;
    
        this.match = this.input.attr('v-match') ? this.input.attr('v-match') : false;
    
       
        this.plupload = this.input.attr('plupload') ? true : false;
        this.dependent = this.input.attr('data-dependent') ? this.input.attr('data-dependent') : false;
        

        if (this.match) {
            this.type = "password_repeat";
        }
    
        if (this.plupload) {
            this.type = "plupload";
        }
        
        switch (this.validate_as) {
            case "checkbox":
                this.response.success = this.input.prop('checked');
                if(!this.response.success) this.val = '';
                break;
            case "exp":
    
                if (!this.isBackKey(evt)) {
                    this.val = this.checkExp(this);
                }
    
                this.input.next("input.exp-year").val('');
                this.input.next("input.exp-month").val('');
    
                var expirationRegex = /(\d\d)\s*\/\s*(\d\d)/;
                var matchIt = expirationRegex.exec(this.val);
                var isValid = false;
                if (matchIt && matchIt.length === 3 && this.val !== "") {
                    var month = parseInt(matchIt[1], 10);
                    var year = parseInt("20" + matchIt[2], 10);
                    if (month >= 0 && month <= 12) {
                        isValid = true;
                        this.input.nextAll("input.exp_year").val(year);
                        this.input.nextAll("input.exp_month").val(month);
                    }
                }
    
                var newInput = this.val.slice(0, 5);
                this.input.val(newInput);
    
                this.response.msg = this.translate.validate_exp;
                if (isValid) {
    
                    this.response.success = true;
                }
    
                break;
            
            case "sum":
    
                this.response.msg = this.translate.validate_sum;
                if (!this.req && this.val === "") {
                    this.response.success = true;
                    break;
                }
    
                this.val = this.val.replace(":", "");
                this.val = this.val.replace("-", "");
    
    
    
                if (this.val.isNumber() && this.val >= 3) {
                    this.response.success = true;
                }
    
                if (!this.val.isNumber()) {
                    this.val = "";
                    this.input.val(this.val);
                }
    
                if (this.val.isNumber()) {
                    if (!isBackKey(evt)) {
                        this.val = this.val + ":-";
                        this.input.val(this.val);
                    }
                }    

                break;
            case "email":
                this.response.msg = this.translate.not_valid_email;
                this.response.success = this.val.isEmail();
            
                break;
            case "password":
                this.response.msg = this.translate.validate_password;
                this.response.success = true;
                break;

            case "password_repeat":
                if ($("#" + match).length < 0 || this.val != $("#" + match).val()) {
                    this.response.msg = this.translate.validate_password_repeat;
                    $("#" + match).parents("div.col").eq(0).addClass("has-error").removeClass("has-success");
                    this.response.success = false;
                } else {
                    $("#" + match).parents("div.col").eq(0).removeClass("has-error").addClass("has-success");
                    this.response.success = true;
                }
                break;
            
            case "date":
                this.response.msg = this.translate.validate_date;
                if (!this.req && this.val === "") {
                    this.response.success = true;
                    break;
                }
                if (this.val.isDate())
                    this.response.success = true;
    
                break;
            case "number":
                this.response.msg = this.translate.validate_number;
                if (!this.req && this.val === "") {
                    this.response.success = true;
                    break;
                }
                if (this.val.isNumber())
                    this.response.success = true;
    
                break;

            case "phone":
                this.response.msg = this.translate.validate_phone;
                if (!this.req && this.val === "") {
                    this.response.success = true;
                    break;
                }
    
                // At least 6 characters with ),(,+,- beeing okay
                var pattern = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})/;
                if( pattern.test( this.val ) ){
                    this.response.success = true;
                }        
            
                break;
            default:
                this.response.success = true;
                break;
        }
        

        this.afterValidation();
    }


    /**
     * The field has been validated, show some (possible) errors
     */
    afterValidation(){
        
        if ((this.req || this.force) && this.val === "") {
            this.response.success = false;
            this.response.msg = this.error_message;
        }

        // Kind of a quick fix for where the error msg shall appear
        let afterElm = this.input;
        
        if (this.type == 'select' || this.validate_as == 'checkbox') {
            afterElm = this.input.next();            
        } else if(this.type == 'div' && this.input.attr('data-checkbox-group')){
            afterElm = this.input.find('.custom-control').last();
        }

        afterElm.next(".form-text").remove();
        
        // Add the validation error message
        if (!this.response.success) {
            this.appendMsg(this.response, afterElm);
            this.input.parents("div.col").eq(0).addClass("has-error").removeClass("has-success");
            return false;
        } else {
            this.input.parents("div.col").eq(0).removeClass("has-error").addClass("has-success");
        }
        
        
        if(this.dependent !== false){

            let dependentEl = this.input.parents('form').find('*[name="'+this.dependent+'"]').eq(0);
            
            if(dependentEl){                    
                if(this.val === ''){
                    dependentEl.removeClass("dep");
                    dependentEl.validate(dependentEl, false);
                } else {
                    dependentEl.addClass("dep");
                    dependentEl.validate(dependentEl, true);
                }
                
            }
            
        }
    }


    /**
     * Is this input a valid input?
     */
    isValid(){
        return this.response.success;
    }

    appendMsg(obj, after) {

        var span = $('<small/>', {
            html: obj.msg,
            class: "form-text text-danger"
        }).attr("data-name", obj.name);
    
        span.insertAfter(after);
    
    }
    

    isBackKey(evt) {
        if (typeof evt !== "undefined") {
            return evt.keyCode == 46 || evt.keyCode == 8 ? true : false;
        }
        return true;
    }
    
    checkExp(el) {
        var inputValue = el.val();
        if (inputValue.length < 4) {
            if (inputValue.length >= 2) {
    
                inputValue = inputValue.replace("/", "");
                var newInput = inputValue.slice(0, 2) + "/" + inputValue.slice(2);
                el.val(newInput);
    
            } else {
                el.val(inputValue);
            }
        }
    
        return el.val();
    }


    static getInstance(args){
        return new GF_Validate(this, args);
    }
}

$.fn.validate = GF_Validate.getInstance;

$(document).on('ready', () => {
    $("body").find(
        "input[data-validate='true'], input[data-required='true'], .g-recaptcha-response, textarea[data-validate='true'], textarea[data-required='true'], select[data-required='true'], div[data-checkbox-group]"
    ).on("change blur", e => {
        // We'll use current target so the checkbox groups work as intended        
        $(e.currentTarget).validate();
    });
});

