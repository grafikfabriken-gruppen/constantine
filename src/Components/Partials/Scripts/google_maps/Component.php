<?php
namespace GF\Components\Partials\Scripts\google_maps;

use GF\Controllers\Template_Controller;
use function GF\Utils\templateController;


final class Component extends \GF\Models\Component{
    
    /**
     * Component with 3rd party settings
     *
     * @var \GF\Components\Partials\Settings\third_party\Component
     */
    public $third_party;

    public function __construct(){
        
        $this->partials = [
            "third_party" => array(
                "id" => "third_party",
                "folder" => "settings"
            )
        ];


        $this->dataTypes["third_party"] = "\\GF\\Components\\Partials\\Settings\\third_party\\Component";
    }

    /**
     * Single run hooks
     *
     * @return void
     */
    public function theme_hooks(){
        $script_name = 'gf_app_js';
        add_filter('localize_script_' . $script_name, array($this, 'localize_script'));
        
        add_action('wp_enqueue_scripts', array($this, 'register_scripts'));


        add_filter('gf_script_depdencies_' . $script_name, array($this, 'depend_script'));
        
        // Add key to ACF
        add_filter('acf/fields/google_map/api', array($this, 'set_acf_google_api_key'));

        // Add required fields
        add_filter('third-party-settings', array($this, 'setup_dependent_contact_fields'));
    }

    /**
     * Setup dependet option fields
     *
     * @param array $fields
     * @return array
     */
    public function setup_dependent_contact_fields($fields){
        $fields[] = 'google_tab';
        $fields[] = 'google_api_key';

        return $fields;
    }

    
    /**
     * Set the google api key for ACF to use as well
     *
     * @param array $api
     * @return array
     */
    public function set_acf_google_api_key($api){
        // Force load the options now
        $this->force_load_options();
        $api['key'] = $this->third_party->google_api_key;

        return $api;
    }


    public function depend_script($scripts){
        $scripts[] = 'google-maps';

        return $scripts;
    }

    

    public function register_scripts(){
        // Force load the options now
        $this->force_load_options();

        $key = $this->third_party->google_api_key ? '?key=' . $this->third_party->google_api_key : '';
        
        wp_register_script('google-maps', 'https://maps.googleapis.com/maps/api/js' . $key, array(), null, false);
    }

    public function localize_script($localize){
        $localize['google_maps_i18n'] = array(
            'default_icon' => true,
            // 'error' => __('Something went wrong', 'grafikfabriken')
        );        

        if(defined('GF_MAP_STYLE')){
            $localize['google_maps_i18n']['styles'] = GF_MAP_STYLE;
        }
        if(defined('GF_MAP_ICON')){
            $localize['google_maps_i18n']['icon'] = GF_MAP_ICON;
        }

        return $localize;
    }
}