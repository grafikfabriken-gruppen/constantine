import $ from "jquery";
/**
 * @author Alexander Widén <alexander@grafikfabriken.nu>
 * How to use
 * <div id="elementid"></div>
 * $("#elementid").googleMap(settings);
 */
class Google_Maps{
    constructor(elm, settings){

        this.$elm = $(elm);
        //App Settings
        settings = $.extend({
            // lat: 59.611030,
            // lng: 16.538258,
            scrollwheel: false,
            zoom: 7,
            map: null,
            disableDefaultUI: false,
            mapType: google.maps.MapTypeId.ROADMAP,
            // icon: 'http://exmple.com/path/to/marker.png', 
            // styles:[{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}],
            center: 0,
            markers: [
                // {
                // lat: 59.611030,
                // lng: 16.538258,
                // marker_content: 'Map marker content'
                // }
            ]
        }, settings);
        this.settings = settings;
        
        
        //Init coordinates for center
        this.settings.centerLocation = new google.maps.LatLng(this.settings.lat, this.settings.lng);

        this.maps_settings = {
            scrollwheel: this.settings.scrollwheel,
            zoom: this.settings.zoom,
            mapTypeId: this.settings.mapType,
            center: this.settings.centerLocation,
            disableDefaultUI: this.settings.disableDefaultUI,
        };

        // Convert from string to json object
        if(this.settings.styles){
            this.maps_settings.styles = JSON.parse(this.settings.styles);
        }

        this.initMap();
        
        // Firefox fix
        google.maps.event.addListenerOnce(this.settings.map, 'idle', () => {
            google.maps.event.trigger(this.settings.map, 'resize');
        });
    }

    /**
     * Calculate the offset for the center
     *
     * @param {float} latlng
     * @param {float} offsetx
     * @param {float} offsety
     * @memberof Google_Maps
     */
    offsetCenter(latlng, offsetx, offsety){
        if (this.settings.map != null) {
            let map = this.settings.map;
            let scale = Math.pow(2, map.getZoom());
            let nw = new google.maps.LatLng(map.getBounds().getNorthEast().lat(), map.getBounds().getSouthWest().lng());

            let worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latlng);
            let pixelOffset = new google.maps.Point((offsetx / scale) || 0, (offsety / scale) || 0);

            let worldCoordinateNewCenter = new google.maps.Point(
                worldCoordinateCenter.x - pixelOffset.x,
                worldCoordinateCenter.y + pixelOffset.y
            );

            let newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

            map.setCenter(newCenter);
        }
    }

    addSingleMarker(){
        // @todo Write this function
    }

    /**
     * Create the map
     *
     * @memberof Google_Maps
     */
    initMap(){

        // Google wants a pure DOM object. No jQuery stuff
        this.settings.map = new google.maps.Map(this.$elm[0], this.maps_settings);
        google.maps.event.addDomListener(window, 'load', this.initMarkers.bind(this));
    }

    /**
     *Initiate the markers that was sent with the creating of the map
     *
     * @returns {void}
     * @memberof Google_Maps
     */
    initMarkers(){
        if (!this.settings.markers.length) return;

        let markers = this.settings.markers;
        for (var i = 0; i < markers.length; i++) {
            let marker = this.createMarker(markers[i]);
        }

        // Set the center of the map
        this.settings.map.setCenter(this.settings.latlng);
        // Save the value for (maybe) later use
        this.settings.center = this.settings.map.getCenter();
        
        this.windowResize();
        $(window).on('resize', this.windowResize.bind(this));
    }

    /**
     * Creates a single marker object
     * 
     * @param {array} _marker 
     */
    createMarker(_marker){

        // Check for specifik marker, otherwise use the default
        let marker_icon = (typeof _marker.icon !== 'undefined') ? _marker.icon : this.settings.icon
        let marker_title = (typeof _marker.title !== 'undefined') ? _marker.title : '';

        // Create the args
        let args = {
            position: {
                lat: _marker.lat,
                lng: _marker.lng
            },
            draggable: false,
            map: this.settings.map,
        };
        
        // Append title if we got one
        if (marker_title) {
            args.title = marker_title;
        }
        // Should we use a specific marker/icon?
        if(marker_icon) {
            args.icon = marker_icon;
        }


        // Append marker to map
        let marker = new google.maps.Marker(args);

        if(_marker.content){
            this.createMarkerPopup(marker, _marker);
        }
        return marker;
    }

    /**
     * Creates a popup bound to the marker
     * 
     * @param {google.maps.Marker} _marker 
     */
    createMarkerPopup(marker, args){
        let marker_content = (typeof args.content !== 'undefined') ? args.content : '';
        
        // Add event listner for the popup (info window)
        marker.addListener('click', () => {
            let infowindow = new google.maps.InfoWindow({
                content: marker_content,
                map: this.settings.map,
            });
            infowindow.open(this.settings.map, marker);
        });
    }


    /**
     * When the window resizes we repostiion the center to still be in... well.. the center
     */
    windowResize(){
        google.maps.event.trigger(this.settings.map, 'resize');
        this.settings.map.setCenter(this.settings.center);
        // this.offsetCenter(this.settings.center, 0, 0);
    }

    /**
     * Creates a single instance of the class
     * @param {object} args 
     */
    static getInstance(args){
        return new Google_Maps(this, args);
    }
}
$.fn.googleMap = Google_Maps.getInstance;



/**
 * Checks the frontend for any maps ready to initiated
 * @author Alexander Widén <alexander@grafikfabriken.nu>
 */
class Google_Map_Initiator{
    constructor(){
        this.google_maps = [];

        this.maps = $('.js-google-map');


        // We got some maps, initiate them
        if (this.maps.length > 0) {
            this.initMaps();
        }

    }

    /**
     * Initiate maps
     */
    initMaps(){

        this.maps.each( (index, map) => {
            let $map = $(map);
            
            let args = {
                scrollwheel: false,
                zoom: 11,
            };
            
            if( $map.data('center') ){
                let center = $map.data('center');
                args.lat = center.lat;
                args.lng = center.lng;
            }

            if( $map.data('zoom') ){
                args.zoom = $map.data('zoom');
            }
            
            if( $map.data('markers') ){
                args.markers = $map.data('markers');
            }
            

            // Global settings
            if(typeof gf_app_js.google_maps_i18n.styles !== 'undefined'){
                args.styles = gf_app_js.google_maps_i18n.styles;
            }
            
            if(typeof gf_app_js.google_maps_i18n.icon !== 'undefined'){
                args.icon = gf_app_js.google_maps_i18n.icon;
            }
            
            $map.googleMap(args);
        });


    }
}

$(document).on('ready', () => {

    let maps = new Google_Map_Initiator();
});