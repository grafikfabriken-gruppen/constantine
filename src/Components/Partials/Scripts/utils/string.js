//Helper to replace all strings!
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

//IsNumber function
String.prototype.isNumber = function () {
    return /^\d+$/.test(this);
};
//IsValidDate
String.prototype.isDate = function () {
    return /^\d{4}-\d{2}-\d{2}$/.test(this);
};

String.prototype.isCivic = function () {
    return /^\d{12}$/.test(this);
};

//isStringEmail
String.prototype.isEmail = function () {
    return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(this);
};