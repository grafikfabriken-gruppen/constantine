require('./fn.jquery');
require('./string');
// import $ from "jquery";

// Helper methods

class GF_Utils{
    constructor(){
        $("html").addClass("grafikfabriken");       
    
        // GF plugins
        this.plugins = {};
        
        // GF plugins drivers
        this.drivers = {}; 
    
        // Get Settings from localize You can populate it with what ever you want
        this.settings = typeof _gf_framework_settings !== 'undefined' ? _gf_framework_settings : {};
    }


    // Trim characters from the beginning and end from a string
    static trim(str, chars) {
        return this.ltrim(this.rtrim(str, chars), chars);
    };

    // Trim characters from the beginning of a string
    static ltrim(str, chars) {
        chars = this.undef(chars) ? ' ' : chars;
        return str.replace(new RegExp('^[' + chars + ']+', 'g'), '');
    };

    // Trim characters from the end of a string
    static rtrim(str, chars) {
        chars = this.undef(chars) ? ' ' : chars;
        return str.replace(new RegExp('[' + chars + ']+$', 'g'), '');
    };

    // Upper Case first letter of string
    static ucFirst(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };

    // Upper Case first letter of each word in string
    static ucWords(str) {
        var words = $.map(str.split(' '), function (s, i) {
            return this.ucFirst(s);
        });
        return words.join(' ');
    };


    // IsChecked?
    static isChecked($el){
        if(!this.undef($el)){
            return $el.is(':checked');
        }
        return false;
    };

    // Sort object by parameter
    static sort(o, key) {
        var list = [],
                x;
        for (x in o) {
            if (o.hasOwnProperty(x)) {
                list.push(o[x]);
            }
        }
        list.sort(function (a, b) {
            return a[key] - b[key];
        });
        return list;
    };

    // Redirect
    static redirect(url) {
        window.location.replace(url);
    };

    // Replace strings like sprintf
    static sprintf(format) {
        var arg = arguments;
        var i = 1;
        if ($.isArray(arg[1])) {
            arg = arg[1];
            i = 0;
        }
        return format.replace(/%((%)|s)/g, function (m) {
            return m[2] || arg[i++];
        });
    };

    // Clear text selection
    static clearSelection() {
        if (document.selection) {
            document.selection.empty();
        } else if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
    };

    // Debug like dpr!
    static debug() {
        if(this.settings.debug == "1"){
            var i, m, a = [];
            for (i = 1, m = arguments.length; i < m; ++i) {
                a.push(arguments[i]);
            }
            if (!this.undef(window.console) && !this.undef(console.log)) {
                if (a.length) {
                    console.log(arguments[0], a);
                } else {
                    console.log(arguments[0]);
                }
            }
        }
    };

    // Disable selection
    static disableSelection(element) {
        var $el = $(element);
        if (!this.undef($el.prop('onselectstart'))) {
            $el.bind('selectstart.disableSelection', function () {
                return false;
            });
        } else if ($el.css('MozUserSelect')) {
            if (this.undef($el.data('css.MozUserSelect'))) {
                $el.data('css.MozUserSelect', $el.css('MozUserSelect'));
            }
            $el.css('MozUserSelect', 'none');
        } else {
            $el.bind('mousedown.disableSelection', function () {
                return false;
            });
        }
        if (this.undef($el.data('css.cursor'))) {
            $el.data('css.cursor', $el.css('cursor'));
        }
        $el.css('cursor', 'default');
    };

    // Enable selection
    static enableSelection(element) {
        var $el = $(element);
        $el.unbind('selectstart.disableSelection');
        $el.unbind('mousedown.disableSelection');
        $el.css('cursor', $el.data('css.cursor'));
        if (!this.undef($el.data('css.MozUserSelect'))) {
            $el.css('MozUserSelect', $el.data('css.MozUserSelect'));
        }
    };

    // Handle cookies
    static cookie(name, value, days) {
        // Write/erase cookie
        if (!this.undef(value)) {
            var expires = '';
            
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = '; expires=' + date.toGMTString();
            }
            document.cookie = escape(name) + '=' + JSON.stringify(encodeURI(value)) + expires + ';path=/;';
        } 

        // Read cookie
        else {
            var nameEQ = escape(name) + '=',
                    ca = document.cookie.split(';'),
                    i, m, c;
            for (i = 0, m = ca.length; i < m; ++i) {
                c = this.trim(ca[i]);
                if (c.indexOf(nameEQ) === 0) {
                    return decodeURI(JSON.parse(c.substring(nameEQ.length)));
                }
            }
            return value; // undefined
        }
    }

    static get_url(key, value, url) {
        if (!url)
            url = window.location.href;
        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
                hash;

        if (re.test(url)) {
            if (typeof value !== 'undefined' && value !== null)
                return url.replace(re, '$1' + key + "=" + value + '$2$3');
            else {
                hash = url.split('#');
                url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
                if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                    url += '#' + hash[1];
                return url;
            }
        } else {
            if (typeof value !== 'undefined' && value !== null) {
                var separator = url.indexOf('?') !== -1 ? '&' : '?';
                hash = url.split('#');
                url = hash[0] + separator + key + '=' + value;
                if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                    url += '#' + hash[1];
                return url;
            } else
                return url;
        }
    };

    static convertToSlug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
        var to = "aaaaaeeeeeiiiiooooouuuunc------";
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

        return str;
    };


     // Generate a random identifier
     static uniqueid(prefix, length) {
        length = length || 32;
        prefix = prefix || '';
        var id = String.fromCharCode(Math.floor((Math.random() * 25) + 65));
        do {
            var code = Math.floor((Math.random() * 42) + 48);
            if (code < 58 || code > 64) {
                id += String.fromCharCode(code);
            }
        } while (id.length < length);
        return prefix + id;
    };

    // Check if variable is undefined.
    static undef(o) {
        return typeof o === 'undefined';
    };

}

export { GF_Utils };