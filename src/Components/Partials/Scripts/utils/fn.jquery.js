import $ from "jquery";


class addRemoveLoad{
    constructor(elm, options){
        options = $.extend({
            add : true
        },options);
        
        if (options.add) {
            elm.addClass("loading");
        } else {
            elm.removeClass("loading");
        }
    }

    static getInstance(args){
        return new addRemoveLoad(this, args);
    }
}
/**
 * $.fn.addRemoveLoad
 * @param {boolean} bool true|false for adding or removing loadClass
 * @returns {undefined}
 * how to use $("element").addRemoveLoad(boolean);
 */
$.fn.addRemoveLoad = addRemoveLoad.getInstance;



class isLoading{
    constructor(elm, options){
        return elm.hasClass("loading");
    }

    static getInstance(args){
        return new isLoading(this, args);
    }
}
/**
 * $.fn.isLoading
 * extended jquery function,
 * @returns {Boolean}
 * how to use $("elemet").isLoading();
 */
$.fn.isLoading = isLoading.getInstance;



class niceRemove{
    constructor(elm, options){
        options = $.extend({
            fadeTime : 300,
            callback : true
        });
        elm.slideUp(options.fadeTime, () => {
            elm.remove();
            if(typeof fn === options.callback) {
                fn(options.callback);
            }
        });
    }

    static getInstance(args){
        return new niceRemove(this, args);
    }
}
/**
 * $.fn.niceRemove
 * @param {Object} options
 * @returns {undefined}
 * how to use $("element").niceRemove({100,callback});
 */
$.fn.niceRemove = niceRemove.getInstance;