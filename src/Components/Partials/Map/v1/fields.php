<?php /** @var \GF\Components\Blocks\Content\v1\Component $this */
return array(
    array(
        'key' => 'block_map_v1_address_coordinates',
        'label' => __("Google map", "grafikfabriken"),
        'name' => 'address_coordinates',
        'type' => 'google_map',        
    ),
    array(
        'key' => 'block_map_v1_zoom',
        'label' => __("Map zoom", "grafikfabriken"),
        'name' => 'zoom',
        'type' => 'range',
        'instructions' => __('Enter a zoom level', 'grafikfabriken'),
        'min' => 0,
        'max' => 20
    ),
);