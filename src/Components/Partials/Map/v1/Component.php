<?php
namespace GF\Components\Partials\Map\v1;

final class Component extends \GF\Models\Component{

    /**
     * Google map
     *
     * @var array
     */
    public $address_coordinates;

    /**
     * Zoom level of the map
     *
     * @var int
     */
    public $zoom;

    
    public function __construct(){
        $this->pretty_name = __('Map', 'grafikfabriken');
        $this->partials = array(
            "google_maps" => array(
                "id" => "google_maps",
                "folder" => "scripts"
            ),
        );
    }

    /**
     * Check if we got coordinates before rendering
     *
     * @return void
     */
    public function render(){
        if(!$this->get_lat() || !$this->get_lng()) return '';


        return parent::render();
    }

    /**
     * Get a unique key for the map
     *
     * @return string
     */
    public function get_map_id(){
        return uniqid('google_map_map-v1-');
    }

    /**
     * Get the zoom level for the map
     *
     * @return int
     */
    public function get_zoom(){
        return $this->zoom;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function get_lat(){
        return isset($this->address_coordinates['lat']) ? $this->address_coordinates['lat'] : 0;
    }

    /**
     * Get longtiude
     *
     * @return float
     */
    public function get_lng(){
        return isset($this->address_coordinates['lng']) ? $this->address_coordinates['lng'] : 0;
    }

}