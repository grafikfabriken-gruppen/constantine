<?php /** @var \GF\Components\Partials\Map\v1\Component $this */?>

<div
  id="<?= $this->get_map_id() ?>"
  data-zoom="<?= $this->get_zoom() ?>"
  data-markers='[{ "lat": <?= $this->get_lat() ?>, "lng": <?= $this->get_lng() ?>}]'
  data-center='{ "lat" : <?= $this->get_lat() ?>, "lng" : <?= $this->get_lng() ?> }' class="google-map js-google-map"
  >
</div>