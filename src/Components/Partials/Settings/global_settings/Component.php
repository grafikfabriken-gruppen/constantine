<?php
namespace GF\Components\Partials\Settings\global_settings;

use function GF\Utils\optionsPageFactory;

final class Component extends \GF\Models\Component
{


    public function __construct(){
        $this->has_options = true;
    }

    public function get_type(){
        $this->type = "Settings";
        return $this->type;
    }

    public function set_options(){
        parent::set_options();
    }

    
    public function installed(){
        // optionsPageFactory()->create_theme_sub_page(__('Global', 'grafikfabriken'), 'global-settings', $this, $this->id);
    }
}