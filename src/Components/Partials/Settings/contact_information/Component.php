<?php
namespace GF\Components\Partials\Settings\contact_information;

use function GF\Utils\optionsPageFactory;

final class Component extends \GF\Models\Component
{

    /**
     * Contact email
     *
     * @var string
     */
    public $email = '';

    /**
     * Contact phone
     *
     * @var string
     */
    public $phone = '';

    /**
     * Contact fax
     *
     * @var string
     */
    public $fax = '';

    /**
     * Contact visiting address
     *
     * @var string
     */
    public $visiting_address = '';

    
    /**
     * Contact visitng postalcode
     *
     * @var string
     */
    public $visiting_postalcode = '';

    /**
     * Contact visiting city
     *
     * @var string
     */
    public $visiting_city = '';


    /**
     * Block title (visible)
     *
     * @var string
     */
    public $title = '';


    /**
     * Visiting location based on a map
     *
     * @var array
     */
    public $visiting_map;



    /**
     * Thank you text for regular contact forms
     *
     * @var string
     */
    public $contact_form_thank_you = '';

    /**
     * Miscellaneous textbox used for theme specific text
     *
     * @var string
     */
    public $miscellaneous_text = '';
    
    /**
     * Who shall receive the contact form emails?
     *
     * @var string
     */
    public $contact_form_reciever = '';

    
    public function __construct(){
        $this->type = "Settings";
        $this->dataTypes["title"] = "string";
        $this->has_options = true;
    }

    /**
     * Get the type. This is needed to trick the bootstrapper
     * 
     * @return string
     */
    public function get_type(){
        $this->type = "Settings";
        return $this->type;
    }

    /**
     * Get formatted phone number with tel:
     *
     * @return string
     */
    public function get_formatted_phone(){
        return sprintf('<a href="tel:%s">%s</a>', $this->phone, $this->phone);
    }
    
    /**
     * Get formatted fax number with tel:
     *
     * @return string
     */
    public function get_formatted_fax(){
        return sprintf('<a href="tel:%s">%s</a>', $this->fax, $this->fax);
    }
    

    /**
     * Get the visiting maps latitude
     *
     * @return float
     */
    public function get_visiting_lat(){
        return $this->visiting_map['lat'];
    }

    /**
     * Get the visiting maps longitude
     *
     * @return float
     */
    public function get_visiting_lng(){
        return $this->visiting_map['lng'];
    }

    /**
     * Get the formatted e-mail link with mailto
     *
     * @return string
     */
    public function get_formatted_email(){
        return sprintf('<a href="mailto:%s">%s</a>', $this->email, $this->email);
    }
    /**
     * Get the formatted miscellaneous text
     *
     * @return string
     */
    public function get_formatted_miscellaneous_text(){
        return apply_filters("the_content", $this->miscellaneous_text);
    }

    /**
     * Get the formatted visiting address (adderss, postal code and city)
     *
     * @return string
     */
    public function get_formatted_visiting_address(){

        // No whitespace allowed since it's going to be formatted by JS Json later
        ob_start();
        ?><p><?= $this->visiting_address ?><br><?= $this->visiting_postalcode . ' ' . $this->visiting_city ?></p><?php 
        return trim(ob_get_clean());
    }

    /**
     * When all components are installed, create a options page
     *
     * @return void
     */
    public function installed(){
        optionsPageFactory()->create_theme_sub_page(__('Contact information', 'grafikfabriken'), 'contact-information-settings', $this);
    }


}