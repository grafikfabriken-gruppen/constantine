<?php

/** @var \GF\Components\Partials\Settings\contact_information $this */

return array(
    // Global contact information
    array(
        'key' => 'option_contact_information_global_tab',
        'label' => __('Global', 'grafikfabriken'),
        'name' => 'option_contact_information_global_tab',
        'type' => 'tab',
        'include_key' => 'global_tab'
    ),
    array(
        'key' => 'option_contact_information_email',
        'label' => __('Email', 'grafikfabriken'),
        'name' => 'option_contact_information_email',
        'type' => 'email',
        'class_key' => 'email'
    ),
    array(
        'key' => 'option_contact_information_phone',
        'label' => __('Phone', 'grafikfabriken'),
        'name' => 'option_contact_information_phone',
        'type' => 'text',
        'class_key' => 'phone'
    ),
    array(
        'key' => 'option_contact_information_fax',
        'label' => __('Fax', 'grafikfabriken'),
        'name' => 'option_contact_information_fax',
        'type' => 'text',
        'class_key' => 'fax'
    ),
    array(
        'key' => 'option_contact_information_miscellaneous_text',
        'label' => __('Miscellaneous text', 'grafikfabriken'),
        'name' => 'option_contact_information_miscellaneous_text',
        'type' => 'textarea',
        'class_key' => 'miscellaneous_text'
    ),


    // Visiting address
    array(
        'key' => 'option_contact_information_visiting_address_tab',
        'label' => __('Visiting address', 'grafikfabriken'),
        'name' => 'option_contact_information_visiting_address_tab',
        'type' => 'tab',
        'include_key' => 'visiting_address_tab'
    ),
    array(
        'key' => 'option_contact_information_visiting_address',
        'label' => __('Address', 'grafikfabriken'),
        'name' => 'option_contact_information_visiting_address',
        'type' => 'text',
        'class_key' => 'visiting_address'
    ),
    array(
        'key' => 'option_contact_information_visiting_postalcode',
        'label' => __('Postalcode', 'grafikfabriken'),
        'name' => 'option_contact_information_visiting_postalcode',
        'type' => 'text',
        'class_key' => 'visiting_postalcode'
    ),
    array(
        'key' => 'option_contact_information_visiting_city',
        'label' => __('City', 'grafikfabriken'),
        'name' => 'option_contact_information_visiting_city',
        'type' => 'text',
        'class_key' => 'visiting_city'
    ),
    array(
        'key' => 'option_contact_information_visiting_map',
        'label' => __('Map', 'grafikfabriken'),
        'instructions' => __('This is used for maps pointing out where they can visit you', 'grafikfabriken'),
        'name' => 'option_contact_information_visiting_map',
        'type' => 'google_map',
        'class_key' => 'visiting_map'
    ),

    // Contact forms
    array(
        'key' => 'option_contact_information_contact_form_tab',
        'label' => __('Contact forms', 'grafikfabriken'),
        'name' => 'option_contact_information_contact_form_tab',
        'type' => 'tab',
        'include_key' => 'contact_form_tab'
    ),
    array(
        'key' => 'option_contact_information_contact_form_thank_you',
        'label' => __('Thank you message', 'grafikfabriken'),
        'instructions' => __('This will be shown when a visitor sends a contact form', 'grafikfabriken'),
        'name' => 'option_contact_information_contact_form_thank_you',
        'type' => 'wysiwyg',
        'class_key' => 'contact_form_thank_you'
    ),
    array(
        'key' => 'option_contact_information_contact_form_reciever',
        'label' => __('E-mail receiver', 'grafikfabriken'),
        'instructions' => __('Who should get the contact form e-mails?', 'grafikfabriken'),
        'name' => 'option_contact_information_contact_form_reciever',
        'type' => 'email',
        'default_value' => get_option('admin_email'),
        'class_key' => 'contact_form_reciever'
    ),
    
);