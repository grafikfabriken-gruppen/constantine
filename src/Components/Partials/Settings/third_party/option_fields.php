<?php

/** @var \GF\Components\Partials\contact_information $this */

return array(
    array(
        'key' => 'option_third_party_google_tab',
        'label' => 'Google',
        'name' => 'option_third_party_google_tab',
        'type' => 'tab',
        'include_key' => 'google_tab'
    ),
    array(
        'key' => 'option_third_party_google_api_key',
        'label' => __('Google API Key', 'grafikfabriken'),
        'name' => 'option_third_party_google_api_key',
        'instructions' => __('Enter the API-key from Google. Please note that the account tied to the key needs to have "Billing" enabled', 'grafikfabriken'),
        'type' => 'text',
        'class_key' => 'google_api_key'
    ),
    array(
        'key' => 'option_third_party_instagram_tab',
        'label' => 'Instagram',
        'name' => 'option_third_party_instagram_tab',
        'type' => 'tab',
        'include_key' => 'instagram_tab'
    ),
    array(
        'key' => 'option_third_party_instagram_page_url',
        'label' => __('Instagram page URL', 'grafikfabriken'),
        'name' => 'option_third_party_instagram_page_url',
        'type' => 'text',
        'class_key' => 'instagram_page_url'
    ),
    array(
        'key' => 'option_third_party_facebook_tab',
        'label' => 'Facebook',
        'name' => 'option_third_party_facebook_tab',
        'type' => 'tab',
        'include_key' => 'facebook_tab'
    ),
    array(
        'key' => 'option_third_party_facebook_page_url',
        'label' => __('Facebook page URL', 'grafikfabriken'),
        'name' => 'option_third_party_facebook_page_url',
        'type' => 'text',
        'class_key' => 'facebook_page_url'
    ),
    array(
        'key' => 'option_third_party_facebook_app_id',
        'label' => __('Facebook App ID', 'grafikfabriken'),
        'name' => 'option_third_party_facebook_app_id',
        'instructions' => __('Enter the App ID for authentication', 'grafikfabriken'),
        'type' => 'text',
        'class_key' => 'facebook_app_id'
    ),
    array(
        'key' => 'option_third_party_facebook_app_secret',
        'label' => __('Facebook App ID', 'grafikfabriken'),
        'name' => 'option_third_party_facebook_app_secret',
        'instructions' => __('Enter the App Secret for authentication', 'grafikfabriken'),
        'type' => 'text',
        'class_key' => 'facebook_app_secret'
    ),
    array(
        'key' => 'option_third_party_facebook_page_id',
        'label' => __('Facebook App ID', 'grafikfabriken'),
        'name' => 'option_third_party_facebook_page_id',
        'instructions' => __('Enter ID of your Facebook page ', 'grafikfabriken'),
        'type' => 'text',
        'class_key' => 'facebook_page_id'
    ),

    // if (!defined('FACEBOOK_APPID'))
    //     define('FACEBOOK_APPID', 604358169756045);
    // if (!defined('FACEBOOK_SECRET'))
    //     define('FACEBOOK_SECRET', "08f8637c418cc30e5f748ccdc0776ece");
    // if (!defined('FACEBOOK_PAGEID'))
    //     define('FACEBOOK_PAGEID', "1441747822738146");
);