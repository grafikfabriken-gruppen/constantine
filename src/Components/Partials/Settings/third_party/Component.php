<?php
namespace GF\Components\Partials\Settings\third_party;

use function GF\Utils\optionsPageFactory;

final class Component extends \GF\Models\Component
{

    /**
     * Google API Key
     * @var string
     */
    public $google_api_key = '';

    
    /**
     * Instagram page URL
     *
     * @var string
     */
    public $instagram_page_url = '';

    /**
     * Facebook page URL
     *
     * @var string
     */
    public $facebook_page_url = '';

    /**
     * Facebook APP ID
     *
     * @var string
     */
    public $facebook_app_id = '';

    /**
     * Facebok APP secret
     *
     * @var string
     */
    public $facebook_app_secret = '';

    /**
     * Facebook page ID
     *
     * @var string
     */
    public $facebook_page_id = '';



    public function __construct(){
        $this->has_options = true;
    }

    public function get_type(){
        $this->type = "Settings";
        return $this->type;
    }

    public function set_options(){
        parent::set_options();
    }

    
    public function installed(){
        optionsPageFactory()->create_theme_sub_page(__('Third party', 'grafikfabriken'), 'third-party-settings', $this, $this->id);
    }
}