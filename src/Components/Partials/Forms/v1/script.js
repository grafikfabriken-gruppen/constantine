import $ from "jquery";

$(document).on('ready', () => {
    $(window).trigger('reloadInputs');
});

$(window).on('reloadInputs', () => {
    const input = $(
        "input[type='text'],input[type='number'],input[type='email'],textarea"
    );
    
    function active(e) {
        e.closest(".form-group").addClass("js-input-active");
    }

    function inactive(e) {
        e.closest(".form-group").removeClass("js-input-active");
    }

    function checkInput($elm) {        
        
        if ($elm.val() != "") {
            active($elm);
        } else {
            inactive($elm);
        }
    }

    input.each( (index, e) => {
        checkInput($(e));
    });

    input.focus(e => {
        active($(e.currentTarget));
    });

    input.blur(e => {

        if ($(e.currentTarget).val() != "") {

        } else {
            inactive($(e.currentTarget));
        }
    });
});
