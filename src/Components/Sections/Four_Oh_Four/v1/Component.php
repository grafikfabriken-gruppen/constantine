<?php
namespace GF\Components\Sections\Four_Oh_Four\v1;

use \GF\Models\Component_PB_Block;

class Component extends Component_PB_Block
{
    /**
     * Title
     * 
     * @var string
     */
    public $title;

    /**
     * Content
     * 
     * @var string
     */
    public $content;

    /**
     * If component is dynamic
     * 
     * @var bool
     */
    public $is_dynamic = true;

    /**
     * If component is dynamic only
     * 
     * @var bool
     */
    public $dynamic_only = true;

    /**
     * The dynamic pages the component is visible on
     * 
     * @var string[]
     */
    public $dynamic_pages = ["404"];

    /**
     * Cache component
     * 
     * @var bool
     */
    public $cache = true;

    /**
     * Link text
     * 
     * @var string
     */
    public $link_text_1;

    /**
     * Link URL
     * 
     * @var string
     */
    public $link_url_1;

    /**
     * If link should be opened in a new window
     * 
     * @var bool
     */
    public $link_new_window_1;

    /**
     * Link text
     * 
     * @var string
     */
    public $link_text_2;

    /**
     * Link URL
     * 
     * @var string
     */
    public $link_url_2;

    /**
     * If link should be opened in a new window
     * 
     * @var bool
     */
    public $link_new_window_2;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pretty_name = __("404", "grafikfabriken");
    }

    /**
     * Do things before rendering
     */
    public function before_render()
    {

        if (!is_string($this->title) || strlen($this->title) < 1) {
            $this->title = __("Error 404", "grafikfabriken");
        }

        if (!is_string($this->content) || strlen($this->content) < 1) {
            $this->content = __("It looks like something went wrong here. The page you are looking for could not be found. Try reloading the page and make sure the URL is correct.", "grafikfabriken");
        }

    }

    /**
     * Get button
     * 
     * @return string
     */
    public function get_button($type=1)
    {

        $button_class = $type == 1 ? 'btn-primary' : 'btn-secondary';

        ob_start();

        if (is_string($this->{"link_url_" . $type}) && strlen($this->{"link_url_" . $type}) > 0 && is_string($this->{"link_text_" . $type}) && strlen($this->{"link_text_" . $type}) > 0) {
            $target = $this->{"link_new_window_" . $type} ? "target=\"_blank\"" : "";

            echo "<a " . $target . " href=\"" . $this->{"link_url_" . $type} . "\" class=\"ml-3 mr-3 btn {$button_class}\">" . $this->{"link_text_" . $type} . "</a>";
        }

        return ob_get_clean();
    }
}