<?php

/** @var \GF\Components\Sections\Four_Oh_Four\v1\Component $block */ ?>

<div class="row justify-content-center">
    <div class="col-12 col-lg-6 text-center">
        <h1><?= $block->title; ?></h1>
        <h4><?= $block->sub_title; ?></h4>
        <?= $block->content; ?>
        <p class="mt-5">
            <?= $block->get_button(); ?><?= $block->get_button(2); ?>
        </p>
    </div>
</div>