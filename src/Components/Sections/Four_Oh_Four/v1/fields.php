<?php

use function \GF\Utils\fieldsFactory;

/** @var \GF\Components\Sections\Four_Oh_Four\v1\Component $this */

$key = "sections_404_v1";

$fields = [
    [
        "key" => $key . "_title",
        "label" => __("Title", "grafikfabriken"),
        "name" => "title",
        "type" => "text",
    ],
    [
        "key" => $key . "_sub_title",
        "label" => __("Sub title", "grafikfabriken"),
        "name" => "sub_title",
        "type" => "text",
    ],
    [
        "key" => $key . "_content",
        "label" => __("Content", "grafikfabriken"),
        "name" => "content",
        "type" => "wysiwyg",
    ],
    [
        "key" => $key . "_link_text_1",
        "label" => __("Button 1 - text", "grafikfabriken"),
        "name" => "link_text_1",
        "type" => "text",
    ],
    [
        "key" => $key . "_link_url_1",
        "label" => __("Button 1 - url", "grafikfabriken"),
        "name" => "link_url_1",
        "type" => "text",
    ],
    [
        "key" => $key . "_link_new_window_1",
        "label" => __("Button 1 - Open link in new window", "grafikfabriken"),
        "name" => "link_new_window_1",
        "type" => "true_false",
    ],
    [
        "key" => $key . "_link_text_2",
        "label" => __("Button 2 - text", "grafikfabriken"),
        "name" => "link_text_2",
        "type" => "text",
    ],
    [
        "key" => $key . "_link_url_2",
        "label" => __("Button 2 - url", "grafikfabriken"),
        "name" => "link_url_2",
        "type" => "text",
    ],
    [
        "key" => $key . "_link_new_window_2",
        "label" => __("Button 2 - Open link in new window", "grafikfabriken"),
        "name" => "link_new_window_2",
        "type" => "true_false",
    ],
];

return fieldsFactory()->get_content_and_settings_fields($key, $fields);