<?php
namespace GF\Components\Base\v1;

use function GF\Utils\optionsPageFactory;


final class Component extends \GF\Models\Component{

    /**
     * Title
     *
     * @var string
     */
    public $title;


    public function __construct(){

    }


    public function render_acf_palette(){
        $colors = json_encode(THEME_PALETTE);
        ?>
            <script>
                acf.add_filter('color_picker_args', args => {
                    args.palettes = <?= $colors ?>;
                    return args;
                });
            </script>
        <?php
    }

    /**
     * Create a global settings page
     *
     * @return void
     */
    public function theme_hooks(){
        if(defined('THEME_PALETTE')){
            add_action('acf/input/admin_footer', array($this, 'render_acf_palette'));
        }
    }

}