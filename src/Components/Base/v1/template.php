<?php

use GF\Controllers\Template_Controller;
use function GF\Utils\templateWrapController;
use function GF\Utils\templateController;

$base = templateController()->get_component_by_type('Base');
$base->set_options();
?>
<!doctype html>
<html id="gf" <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> role="document">
    <?php wp_body_open(); ?>
    <?= templateController()->get_preloader(); ?>
    <?= templateController()->get_header(); ?>
    <?php include templateWrapController()->get_main_template(); ?>
    <?= templateController()->get_footer(); ?>
    <?= templateController()->get_outdated_browser(); ?>
    <?= templateController()->get_debug(); ?>
    <?php wp_footer(); ?>
</body>

</html>