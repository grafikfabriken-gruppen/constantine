<section class="gf page_builder_password_form">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4><?= __('Please enter the password', 'grafikfabriken'); ?></h4>
                <?php
                $post   = get_post();
                $label  = 'pwbox-' . (empty($post->ID) ? rand() : $post->ID);
                ?>
                <form action="<?= site_url('wp-login.php?action=postpass', 'login_post'); ?>" class="post-password-form" method="post">
                    <p><?= __('This content is password protected. To view it please enter your password below:'); ?></p>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="<?= $label; ?>"><?= __('Password', 'grafikfabriken')?></label>
                            <input name="post_password" class="form-control" id="<?= $label; ?>" type="password" placeholder="<?= __('Password', 'grafikfabriken'); ?>" size="20" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <input type="submit" class="btn btn-primary" name="Submit" value="<?= __('Login', 'grafikfabriken'); ?>" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>