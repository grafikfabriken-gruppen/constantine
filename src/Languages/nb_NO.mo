��    v      �  �   |      �	     �	     
     
  
   :
     E
     d
  
   l
     w
     �
     �
     �
  Z   �
     "     5     P     `     l     q     v     |     �     �     �     �     �     �  	   �  	   �     �     �  	   �                     9     @     O     _     e     �  #   �  '   �     �     �          
           -     ?     K     Y     h     o  
   ~  
   �     �     �  	   �  	   �     �     �     �     �     �  	   �                           	   '     1     5     >     C     K     S     f     k     p     �     �     �     �     �  	   �  
   �  
   �     �  	   �  	   �     �     �                 )   1     [     g     |     �     �     �     �     �  ;   �  d   	  6   n  !   �     �     �     �     �     �     �       	   4  �  >     �     �     �       ,   !     N     V     _     n     }     �  W   �          '     =     O     [     `     c     i     o     w     �     �     �     �  	   �  	   �     �     �     �     �               )     0     ?     O     V  	   l  !   v  %   �     �     �     �     �     �     	     !     .     B     Y     `     s       
   �  
   �     �     �     �     �     �     �     �  
   �                    %     +  	   0     :  	   ?     I     N     V     ]     k     p     x     �     �     �     �     �  	   �  
   �  
   �  
   �  
   �     �     �     �               &  $   C  
   h     s  
   �     �     �     �     �     �  5   �  X     3   u  !   �     �  	   �     �     �     �     �           8     q   L   c   Q   _   H   v   D   "       C   `   ;   <       #           e   /   A       5   %   G          >      h   3   a       K      d   s   7      1   R   \       ^                    2           S      B   X       (      Z   l          *   k         =      
       @              	   u   N   P   &      O   ]   n          6       +   g             :   T   $   U   ,         9   M   p   )   o   b           8   V       r      J   i           f                  [   .           m         E                     4      F      j   0   Y           W   -   ?   !           I   '               t    50/50 Double content 50/50 Image and content 50/50 Image+map and content Add column Add extra CSS-classes to block Address Background Background color Background image Block background color Block background image Block id must be unique, Use for targeting blocks like this: http://mypage.com/#example_id Build your content Build your dynamic content Button settings CSS-classes Card City Close Color Contact Contact form Contact forms Contact information Contacts Content Content 1 Content 2 Copyright text Coworker Coworkers Dynamic content Dynamic views Dynamicly show the content E-mail E-mail address E-mail receiver Email Enter ID of your Facebook page  Enter a zoom level Enter the App ID for authentication Enter the App Secret for authentication Facebook App ID Facebook page URL Fax Full height on block? Full height? Full width block? Full width? General block General blocks Global Google API Key Google Map Google map Header Heading Heading 1 Heading 2 Hero Image Information Instagram page URL Internal section name Job title Label Left Left content Link Logotype Main menu Map Map zoom Menu Message Mirror? Miscellaneous text Name News Number of posts Phone Phone exchange Post Post: archive Post: category Post: tag Postalcode Production Question Questions Read more Right Right content Send message Settings Settings for block Should the content and image swap places? Single post Something went wrong Sub heading Subject Text Thank you message Theme settings This field is required This is used for maps pointing out where they can visit you This is used to keep track of your sections by giving them a personal name. Visitors never sees this This will be shown when a visitor sends a contact form This will show the latest n posts Three columns Timeline Title Visiting address Year You must choose an option You need to enter a valid email Your name Project-Id-Version: 
POT-Creation-Date: 2019-06-27 16:22+0200
PO-Revision-Date: 2019-06-28 16:19+0200
Last-Translator: 
Language-Team: 
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_x:1,2c
X-Poedit-SearchPath-0: .
 50/50 Dobbelt innhold 50/50 bilde og innhold 50/50 Bilde+kart og innhold Legg til kolonne Legg til ekstra CSS-klasser til blokkeringer Adresse Bakgrunn Bakgrunnsfarge Bakgrunnsbilde Bakgrunnsfarge for blokkeringer Bakgrunnsbilde for blokkeringer Blokkering må være unik. Bruk for og spore blokkeringer http://mypage.com/#example_id Bygg ditt innhold Bygg dynamisk innhold Innstilling knapp CSS-klasser Kort By Steng Farge Kontakt Kontaktskjema Kontaktskjema Kontaktinformasjon Kontakt Innhold Innhold 1 Innhold 2 Skrivebeskyttet tekst Kollega Kollega Dynamisk innhold Dynamisk visning Vis innholdet dynamisk E-post E-post adresse E-post mottager E-post Skriv inn Facebook ID Angi zoom Skriv inn App ID for autorisasjon Skriv inn App Secret for autorisasjon Facebook App ID Facebook page URL Fax Full høyde blokkering? Full høyde? Full bredde blokkering? Full bredde? Generell blokkering Generelle blokkeringer Global Google API nøkkel Google Maps Google Maps Overskrift Overskirft Overskrift 1 Overskrift 2 Hero Bilde Informasjon Instagram page URL Internt seksjonsnavn Jobbtittel Etikett Venstre Innhold venstre Lenke Logo Hovedmeny Kart Kart zoom Meny Melding Speil? Diverse tekst Navn Nyheter Antall poster Telefon Telefon: Post Post: arkiv Post: kategori Post: tag Postnummer Produksjon Spørsmål Spørsmål Les mer Høyre Innhold høyre Send melding Innstillinger Innstillinger for blokkering Burde innhold og bilde byttet plass? Enkel post Noe gikk galt Overskirft Emne Tekst "Takk for henvendelsen" beskjed Temainnstillinger Dette feltet er obligatorisk Dette benyttes for og vise besøksadressen på kartet Dett er for og spore seksjoner ved og gi dem personlige navn. Besøkende ser aldri dette Dette vises når en bruker sender inn kontaktskjema Dette vil vise den siste n posten Tre kolonner Tidslinje Tittel Besøksadresse År Du må velge et alternativ Fyll inn godkjent e-post adresse Dit navn 