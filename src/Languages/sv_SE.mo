��    `        �         (     )  	   -     7     V     ^  
   j     u     �  Z   �     �     	  "   ,	     O	     _	  "   n	     �	     �	  �   �	     G
     S
     p
     u
     �
     �
     �
     �
     �
  	   �
  	   �
     �
     �
     �
     �
               (  k   ;  #   �  '   �  	   �     �               #     *     @     M     _     k     y     �     �  
   �     �  	   �  	   �     �     �  �   �     �     �  	   �     �     �     �     �     �     �     �     �     �  	     
     
        $     (     1  	   D     N     Z  	   o     y     �     �     �  P   �       ;   +  d   g  6   �          	  (        C     ]  (  }     �     �  '   �     �  
   �     �             o   )     �     �  &   �     �     �  &        4     C  5   Q     �     �     �     �     �     �     �     �  	   �                     '     ?     N     ^  "   e     �  u   �       (   -     V     g     w     �     �     �     �     �  	   �     �     �     �               #     ,     5     C     ]  ,   r     �     �  	   �     �  	   �     �     �  	   �     �     �               &  
   4  
   ?     J     N     ]  	   w     �     �     �     �     �     �  
   �  U   �  "   G  ?   j  8   �  I   �     -     4  #   B     f  %   �         J   =                                4   R   I   _       Q   F           :   Z            .   M   Y       L   X   0   ]   )   S   B   @      6       2   T   N              
             "   8   %   '   	   ,   5   H               W   P   (   9              7   ^                        >                     K   G              E      [   U                    &   #       V         \   ;      1   `          -       A      D          *       3   /   <   C      O   !           +   ?   $    404 Add block Add extra CSS-classes to block Address Anchor link Background Block background color Block background image Block id must be unique, Use for targeting blocks like this: http://mypage.com/#example_id Build your content Build your dynamic content Button 1 - Open link in new window Button 1 - text Button 1 - url Button 2 - Open link in new window Button 2 - text Button 2 - url By using this field it will enable you to link to this block with hashtags like this: http://yoursite.com/#my_awesome_block. Formatting only a-z and _ CSS-classes Choose title tag for field:  City Clear cache Close Contact Contact forms Contact information Content Content 1 Content 2 Copyright text Default Dynamic views E-mail receiver Email Enter ID of your Facebook page  Enter a zoom level Enter the API-key from Google. Please note that the account tied to the key needs to have "Billing" enabled Enter the App ID for authentication Enter the App Secret for authentication Error 404 Facebook App ID Facebook page URL Fax Footer Full height on block? Full height? Full width block? Full width? General block General blocks Global Google API Key Google map Header Heading 1 Heading 2 Instagram page URL Internal section name It looks like something went wrong here. The page you are looking for could not be found. Try reloading the page and make sure the URL is correct. Login Logotype Main menu Map Map zoom Menu Miscellaneous text Password Phone Please enter the password Post: archive Post: category Post: tag Postalcode Production SEO Settings Settings for block Shortcode Single post Something went wrong Sub title Thank you message Theme cache has been cleared Theme settings Third party This content is password protected. To view it please enter your password below: This field is required This is used for maps pointing out where they can visit you This is used to keep track of your sections by giving them a personal name. Visitors never sees this This will be shown when a visitor sends a contact form Title Visiting address Who should get the contact form e-mails? You must choose an option You need to enter a valid email Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-KeywordsList: __;_x:1,2c;_e;_n
X-Poedit-Basepath: ../..
X-Poedit-SearchPath-0: .
 404 Lägg till sektion Lägg till extra CSS-klasser till block Adress Ankarlänk Bakgrund Block bakgrundsfärg Block bakgrundsbild Block-ID måste vara unikt. Använd detta för att komma åt block, exempelvis: https://example.com/#example_id Bygg ert innehåll Bygg ert dynamiska innehåll Knapp 1 - Öppna länk i nytt fönster Knapp 1 - text Knapp 1 - url Knapp 2 - Öppna länk i nytt fönster Knapp 1 - text Knapp 2 - url Ge denna sektion en ankarlänk. Anges endast med a-z. CSS-klasser Välj titelns tagg för fält:  Ort Rensa cache Stäng Kontakt Kontaktformulär Kontaktinformation Innehåll Innehåll 1 Innehåll 2 Copyright-text Standard för sektionen Dynamiska vyer E-postmottagare E-post Ange ID’t för er Facebook-sida  Ange zoom-nivå Ange API-nyckel från Google. Vänligen notera att kontot associerat med nyckeln måste ha “debitering” påslaget Ange App-ID för autentisering Ange “App Secret” för autentisering Sidan finns inte Facebook App-ID Facebook App-ID Fax Sidfot Full höjd på block? Full höjd? Fullbrett block? Fullbred? Återkommande sektion Återkommande sektioner Global Google API-Nyckel Google karta Sidhuvud Rubrik 1 Underrubrik 1 Instagrams URL för sidan Internt sektionsnamn Något gick fel, sidan du sökte finns inte. Login Logotyp Huvudmeny Karta Kart-zoom Meny Övrig text Lösenord Telefonnummer Ange ett lösenord Inlägg: Arkiv Inlägg: Kategorier Inlägg: Tagg Postnummer Produktion SEO Inställningar Inställningar för block Shortcode Enskilt inlägg Något gick fel Underrubrik Tackmeddelande Tema-cachen är rensad Temainställningar Tredjepart Detta innehåll är lösenordsskyddat. För att se det ange ditt lösenord nedanför. Det här fältet är obligatoriskt Det här används för att i kartor kunna visa er besöksadress Används för administratörer för att särskilja block Det här kommer att visas när en besökare fyller i ett kontaktformulär Rubrik Besöksadress Vem ska få detta kontaktformulär? Du måste välja ett alternativ Du måste ange en giltig e-postadress 