<?php

namespace GF\Factories;

use GF\Utils\Singleton;
use function GF\Utils\builderBlockFactory;
use function GF\Utils\pageBuilder;

class Fields_Factory extends Singleton
{

    /**
     * Setup all fields for our page builder
     *
     * @return void
     */
    public function setup()
    {

        //Add clone fields
        $this->clone_fields();

        //Add settings fields
        $this->settings_fields();

        //Add tab fields
        $this->tab_fields();

        //Add builder fields
        $this->builder_fields();

        //Add dynamic views fields
        // $this->dynamic_views_fields();

    }

    /**
     * Generate a builder block fields
     * for page builder
     *
     * @param string $name
     * @param string $key
     * @param string $fields
     * @return array
     */
    public function buildBlockFields($name, $key, $fields){
        // return $fields;
        return [[
            'title' => $name,
            'key' => $key,
            'type' => 'hidden',
        ]];
    }

    /**
     * Add dynamic views fields
     *
     * @return void
     */
    // public function dynamic_views_fields(){

    //     if(!\array_has_items(pageBuilder()->dynamic_views)) return;

    //     foreach (pageBuilder()->dynamic_views as $key => $value) {
    //         $builder_blocks = array(
    //             'key' => $value . '_group',
    //             'title' => __('Build your content', "grafikfabriken"),
    //             'fields' => array(
    //                 array(
    //                     'key' => $value,
    //                     'label' => '',
    //                     'name' => $value,
    //                     'type' => 'flexible_content',
    //                     'instructions' => '',
    //                     'required' => 0,
    //                     'conditional_logic' => 0,
    //                     'wrapper' => array(
    //                         'width' => '',
    //                         'class' => '',
    //                         'id' => '',
    //                     ),
    //                     'layouts' => array(),
    //                     'button_label' => __('Add block', "grafikfabriken"),
    //                     'min' => '',
    //                     'max' => '',
    //                 ),
    //             ),
    //             'location' => array(
    //                 array(
    //                     array(
    //                         'param' => 'options_page',
    //                         'operator' => '==',
    //                         'value' => 'gf-pb-settings-' . $value,
    //                     ),
    //                 ),
    //             ),
    //             'menu_order' => 0,
    //             'position' => 'normal',
    //             'style' => 'default',
    //             'label_placement' => 'top',
    //             'instruction_placement' => 'label',
    //             'hide_on_screen' => array(
    //                 0 => 'the_content',
    //             ),
    //             'active' => 1,
    //             'description' => '',
    //         );

    //         builderBlockFactory()->builder_fields_templates[$key] = $builder_blocks;

    //     }
    // }

    /**
     * Add builder fields
     *
     * @return void
     */
    public function builder_fields(){
        builderBlockFactory()->builder_fields = array(
            'key' => pageBuilder()->name . '_group',
            'title' => __('Build your content', "grafikfabriken"),
            'fields' => array(
                array(
                    'key' => pageBuilder()->name,
                    'label' => '',
                    'name' => pageBuilder()->name,
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'layouts' => array(),
                    'button_label' => __('Add block', "grafikfabriken"),
                    'min' => '',
                    'max' => '',
                ),
            ),
            'location' => array(
                array(),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => array(
                0 => 'the_content',
            ),
            'active' => 1,
            'description' => '',
        );
    }

    /**
     * Get title tag field
     *
     * @param string $key
     * @param string $name
     * @param string $label
     * @param string $tag
     * @return array
     */
    public function get_title_tag_field($key, $name, $label, $tag): array
    {
        return array(
            'key' => $key . '_tag',
            'label' => __('Choose title tag for field: ', 'grafikfabriken') . $label,
            'name' => $name . "_tag",
            'type' => 'select',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'choices' => array(
                'default' => __('Default', 'grafikfabriken'),
                'h1' => 'h1',
                'h2' => 'h2',
                'h3' => 'h3',
                'h4' => 'h4',
                'h5' => 'h5',
                'h6' => 'h6',
            ),
            'default_value' => array(
                0 => 'default',
            ),
            'allow_null' => 0,
            'multiple' => 0,
            'ui' => 0,
            'return_format' => 'value',
            'ajax' => 0,
            'placeholder' => ''
        );
    }

    /**
     * Generate tag field
     *
     * @param array $field
     * @return array
     */
    public function generate_tag_field(array $field): array
    {

        $response = array();

        if (\array_has_items($field) && isset($field["custom_title_tag"]) && $field["custom_title_tag"]) {
            $response = $this->get_title_tag_field($field["key"], $field["name"], $field["label"], $field["custom_title_tag"]);
        }

        return $response;
    }

    /**
     * Get Generic setting fields
     *
     * @param string $key
     * @param array $additional_fields
     * @return array
     */
    public function get_content_and_settings_fields($key, $content_fields = array(), $additional_settings_fields = array(), $custom_settings_on_field = false, $enable_settings = true): array
    {

        $mapped_content_fields = array();
        if (\array_has_items($content_fields) && $enable_settings) {
            $mapped_content_fields = array_merge(
                array(
                    array(
                        'key' => $key . '_content_tab',
                        'label' => __('Content', 'grafikfabriken'),
                        'type' => 'tab',
                        'placement' => 'left',
                    )
                ),
                $content_fields
            );
        } else {
            $mapped_content_fields = $content_fields;
        }


        $seo_fields = array();

        if (\array_has_items($content_fields) && $custom_settings_on_field) {

            $temp_seo_fields = array(
                array(
                    'key' => $key . '_seo_tab',
                    'label' => __('SEO', 'grafikfabriken'),
                    'type' => 'tab',
                    'placement' => 'top',
                )
            );

            $add_fields = false;
            $old_content_fields = $content_fields;
            $content_fields = array();

            foreach ($old_content_fields as $k => $field) {
                $content_fields[] = $field;

                $field2 = $this->generate_tag_field($field);

                if (\array_has_items($field2)) {
                    $add_fields = true;
                    if ($enable_settings) {
                        $temp_seo_fields[] = $field2;
                    } else {
                        $content_fields[] = $field2;
                    }
                }
            }

            if ($add_fields && $enable_settings) {
                $seo_fields = $temp_seo_fields;
            } else if ($add_fields && !$enable_settings) {
                $mapped_content_fields = $content_fields;
            }
        }

        $settings_fields = $enable_settings ? array_merge(array(
            array(
                'key' => $key . '_settings_tab',
                'label' => __('Settings', 'grafikfabriken'),
                'type' => 'tab',
                'placement' => 'left',
            ),
            array(
                'key' => $key . '_settings_html_id',
                'label' => __('Anchor link', 'grafikfabriken'),
                'instructions' => __("By using this field it will enable you to link to this block with hashtags like this: http://yoursite.com/#my_awesome_block. Formatting only a-z and _", 'grafikfabriken'),
                'name' => 'html_id',
                'type' => 'text'
            ),
            array(
                'key' => $key . '_settings_internal_section_name',
                'label' => __('Internal section name', 'grafikfabriken'),
                'instructions' => __('This is used to keep track of your sections by giving them a personal name. Visitors never sees this', 'grafikfabriken'),
                'name' => 'internal_section_name',
                'type' => 'text'
            )
        ), $additional_settings_fields) : array();

        return array_merge(
            $mapped_content_fields,
            array_merge(
                $settings_fields,
                $seo_fields
            )
        );
    }

    /**
     * Add settings fields
     *
     * @return void
     */
    public function settings_fields(){

        builderBlockFactory()->defualt_fields["settings"] = array(
            'key' => 'gf_builder_settings',
            'label' => __('Settings for block', 'grafikfabriken'),
            'name' => '',
            'display' => 'block',
            'fields' => array(
                array(
                    'key' => 'settings_id',
                    'label' => 'ID',
                    'name' => 'html_id',
                    'type' => 'text',
                    'instructions' => __('Block id must be unique, Use for targeting blocks like this: http://mypage.com/#example_id', "grafikfabriken"),
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'settings_background_image',
                    'label' => __('Block background image', "grafikfabriken"),
                    'name' => 'background_image',
                    'type' => 'image',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '50',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'id',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array(
                    'key' => 'settings_background_color',
                    'label' => __('Block background color', "grafikfabriken"),
                    'name' => 'background_color',
                    'type' => 'color_picker',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '50',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                ),
                array(
                    'key' => 'settings_full_width',
                    'label' => __('Full width?', "grafikfabriken"),
                    'name' => 'full_width',
                    'type' => 'true_false',
                    'instructions' => __('Full width block?', "grafikfabriken"),
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '50',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => 'Ja',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
                array(
                    'key' => 'settings_full_height',
                    'label' => __('Full height?', "grafikfabriken"),
                    'name' => 'full_height',
                    'type' => 'true_false',
                    'instructions' => __('Full height on block?', "grafikfabriken"),
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '50',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => 'Ja',
                    'default_value' => 0,
                    'ui' => 0,
                    'ui_on_text' => '',
                    'ui_off_text' => '',
                ),
                array(
                    'key' => 'settings_css',
                    'label' => __('CSS-classes', "grafikfabriken"),
                    'name' => 'css',
                    'type' => 'text',
                    'instructions' => __('Add extra CSS-classes to block', "grafikfabriken"),
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '50',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'post',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 0,
            'description' => '',
        );

    }

    /**
     * Clone fields
     *
     * @return void
     */
    public function clone_fields()
    {
        builderBlockFactory()->clone_fields = array(
            array(
                'key' => 'settings',
                'label' => 'klon',
                'name' => 'klon',
                'type' => 'clone',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'clone' => array(
                    0 => 'gf_builder_settings',
                ),
                'display' => 'seamless',
                'layout' => 'block',
                'prefix_label' => 0,
                'prefix_name' => 0,
            )
        );
    }

    /**
     * Tab fields
     *
     * @return void
     */
    public function tab_fields(){

        builderBlockFactory()->defualt_fields["block_tab"] = array(array(
            'key' => 'builder_block_tab',
            'label' => __('Content', "grafikfabriken"),
            'name' => '',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placement' => 'left',
            'endpoint' => 0,
        ));

        builderBlockFactory()->defualt_fields["settings_tab"] = array(array(
            'key' => 'builder_settings_tab',
            'label' => __('Settings', "grafikfabriken"),
            'name' => '',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placement' => 'left',
            'endpoint' => 0,
        ));
    }

}