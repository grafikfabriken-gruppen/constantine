<?php
namespace GF\Factories;

use GF\Utils\Singleton;
use function GF\Utils\pageBuilder;

class Options_Page_Factory extends Singleton{

    /**
     * Theme settings slug
     * 
     * @var string
     */
    public $theme_settings_slug = 'gf-theme-settings';

    /**
     * Dynamic views slug
     *
     * @var string
     */
    public $dynamic_viwes_slug = 'gf-pb-settings';

    /**
     * Store options pages
     *
     * @var array
     */
    public $option_pages = [];

    /**
     * Add options page for our active templates
     *
     * @return void
     */
    public function setup(){

        acf_add_options_page(array(
            'page_title' => __("Theme settings", "grafikfabriken"),
            'menu_title' => __("Theme settings", "grafikfabriken"),
            'menu_slug' => $this->theme_settings_slug,
            'capability' => 'edit_posts',
            'redirect' => true,
            'icon_url' => 'dashicons-admin-settings'
        ));


        if(!array_has_items(pageBuilder()->dynamic_views)) return;

        acf_add_options_page(array(
            'page_title' => __("Dynamic views", "grafikfabriken"),
            'menu_title' => __("Dynamic views", "grafikfabriken"),
            'menu_slug' => $this->dynamic_viwes_slug,
            'capability' => 'edit_posts',
            'redirect' => true,
            'icon_url' => 'dashicons-editor-table'
        ));

        foreach (pageBuilder()->dynamic_views as $key => $value) {

            $name = $key;

            switch($key){
                case 'archive':
                    $name = __('Post: archive', "grafikfabriken");
                    break;
                case 'category':
                    $name = __('Post: category',"grafikfabriken") ;
                    break;
                case 'tag':
                    $name = __('Post: tag',"grafikfabriken");
                    break;
                case 'single-post':
                    $name = __('Single post',"grafikfabriken");
                    break;
            }

            $name = apply_filters('dynamic_view_name', $name);
            $view_menu_name = apply_filters('dynamic_view_menu_name', $this->dynamic_viwes_slug . '-' . $value);


            acf_add_options_sub_page(array(
                'page_title' => ucfirst($name),
                'menu_title' => ucfirst($name),
                'menu_slug' => $view_menu_name,
                'capability' => 'edit_posts',
                'redirect' => true,
                'parent_slug' => $this->dynamic_viwes_slug,
            ));

            
        }

    }

    /**
     * Create a theme page settings
     *
     * @param string $name
     * @param string $value
     * @param array|Component $fields
     * @param string $caps
     * @return void
     */
    public function create_theme_sub_page($name, $value, $fields, $id = null, $caps = 'edit_posts'){
        
        if(is_a($fields, '\\GF\Models\\Component') && $fields->type === "Settings"){

            $new_fields = [];

            $fields->options_in_use = array_unique(apply_filters($value, array()));

            foreach ($fields->option_fields as $key => $field) {
                if(isset($field["class_key"]) && in_array($field["class_key"], $fields->options_in_use)){
                    $new_fields[$key] = $field;
                }elseif (isset($field["include_key"]) && in_array($field["include_key"], $fields->options_in_use)) {
                    $new_fields[$key] = $field;
                }
            }

            $fields = $new_fields;
        }


        $menu_slug = $this->theme_settings_slug . '-' . $value;

        acf_add_options_sub_page(array(
            'page_title' => ucfirst($name),
            'menu_title' => ucfirst($name),
            'menu_slug' => $menu_slug,
            'capability' => $caps,
            'redirect' => true,
            'parent_slug' => $this->theme_settings_slug,
        ));

        if(array_has_items($fields)){
            if(!isset($fields["fields"])){

                $fields = array(
                    'key' => $value,
                    'title' => $name,
                    'fields' => $fields,
                    'location' => array(
                        array(
                            array(
                                'param' => 'options_page',
                                'operator' => '==',
                                'value' => $menu_slug,
                            ),
                        ),
                    ),
                    'menu_order' => 0,
                    'position' => 'normal',
                    'style' => 'default',
                    'label_placement' => 'top',
                    'instruction_placement' => 'label',
                    'hide_on_screen' => '',
                    'active' => 1,
                    'description' => '',
                );

            }

            acf_add_local_field_group($fields);

            //Save this if wee have an id!
            if($id !== null){
                $this->option_pages[$id] = $menu_slug;
            }
            

        }
    }

}