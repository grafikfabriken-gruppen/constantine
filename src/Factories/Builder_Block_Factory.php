<?php
namespace GF\Factories;

use GF\Utils\Singleton;

class Builder_Block_Factory extends Singleton
{   
    /**
     * Active blocks
     *
     * @var array
     */
    public $blocks = [];

    /**
     * Builder fields
     *
     * @var array
     */
    public $builder_fields;

    /**
     * Builder fields templates
     *
     * @var array
     */
    public $builder_fields_templates = [];

    /**
     * Default fields
     *
     * @var array
     */
    public $default_fields = [];
    public $defualt_fields = [];

    /**
     * Clone fields
     *
     * @var array
     */
    public $clone_fields = [];

    /**
     * Post types
     *
     * @var array
     */
    public $post_types = [];

    /**
     * Blocks to be added per dynamic view
     *
     * @var array
     */
    public $dynamic_builder = array();

    /**
     * Sections with screenshots
     *
     * @var array
     */
    public $sections_with_screenshots = array();

    public function __construct()
    {   

        /**
         * This fixes the posibility
         * to add blocks to specific post types
         * 
         */
        global $post, $post_type;

        $valid_backend_post_pages = array('post.php', 'post-new.php');
        $is_edit_page = isset($_SERVER["SCRIPT_NAME"]) && in_array(basename($_SERVER["SCRIPT_NAME"]), $valid_backend_post_pages);

        if ($is_edit_page) {

            //Set up post
            if (isset($_GET['post']) && (int) sanitize_text_field($_GET['post']) > 0) {
                $_temp = get_post((int) sanitize_text_field($_GET['post']));
                if ($_temp && $_temp->ID > 0) {
                    $post = $_temp;
                    $post_type = $post->post_type;
                }
            } else if (isset($_GET['post_type'])) {
                $post_type = sanitize_text_field($_GET['post_type']);
            }
        }

        add_action('acf/init', array($this, 'generate_backend_builder'), 999);
        add_filter('acf/fields/flexible_content/layout_title', array($this, 'display_screenshot'), 10, 4);
    }

    /**
     * Display screenshots
     *
     * @param string $title
     * @param array $field
     * @param array $layout
     * @param index $i
     * @return void
     */
    public function display_screenshot($title, $field, $layout, $i){
        
        if($new_title = get_sub_field("internal_section_name")){
            $title = '<span alt="'.$title.'">' . $new_title . '</span>';
        }

        if(isset($layout["key"]) && in_array($layout["key"], array_keys($this->sections_with_screenshots))){
            // dpr($layout);
            $style = "position: relative;display: inline-block;top: 12px;margin-top: -30px;margin-left: 5px;border: 1px solid #eee;padding: 2px;line-height: 0px;box-shadow: 0px 1px 7px 0px #eee;margin-right: 10px;";
            $link = $this->sections_with_screenshots[$layout["key"]];

            $title = sprintf( '<div class="thumbnail" style="%s"><a href="%s" target="_blank"><img height="33" src="%s"></a></div>', $style, $link, $link) . $title;
        }

        //Add has tag
        if ($bname = get_sub_field('html_id')) {

            global $post;
            if (is_a($post, 'WP_Post')) {
                $link = get_the_permalink($post) . '#bname';
            } else {
                $link = home_url('#' . $bname);
            }

            $title = $title . sprintf(' - <span style="color:#999;"><a target="_blank" style="text-decoration: none;" href="%s">#%s<span style="position:relative; top:1px; font-size: 15px; vertical-align: middle;" class="dashicons dashicons-external"></span></a></span>', $link, $bname);
        }

        return $title;
    }

    /**
     * Show screenshot in backend
     *
     * @param string $key
     * @param  \GF\Models\Component_PB_Block $component
     * @return void
     */
    public function add_screenshot($key, $component){
        
        $screenshot = $component->get_screenshot();
        if($screenshot !== ''){
            $this->sections_with_screenshots[$key] = $screenshot;
        }

    }

    /**
     * Set post types
     *
     * @param array $post_types
     * @return void
     */
    public function set_post_types($post_types){
        $this->post_types = array_has_items($post_types) ? $post_types : array();
    }

    /**
     * Get a block by name
     *
     * @param array $data_block
     * @return \GF\Models\Component
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    public function get_block_by_name($data_block)
    {

        $slug = $data_block["acf_fc_layout"];

        if (isset($this->blocks[$slug])) {
            
            /** @var GF\Models\Component_PB_Block $block */
            $block = $this->blocks[$slug];

            //For pagebuilder blocks with a options page
            $block->set_options();

            \GF\Controllers\Template_Controller::getInstance()->install_global_partials($block);
            
            $data = \GF\Utils\Array_Utils::map_data_to_component($data_block, $block);

            return $data;
        }

        return false;

    }

    /**
     * Add block
     *
     * @param \GF\Models\Component_PB_Block $_settings
     * @return void
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    public function add_block($id, $component)
    {   
        if(!isset($this->blocks[$id]) && is_object($component)){
            $this->blocks[$id] = $component;
        }
    }

    /**
     * Generate PageBuilder
     *
     * @return void
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    public function generate_backend_builder()
    {

        global $post_type;
        $local_post_type = $post_type && is_string($post_type) ? $post_type : null;

        $location = array();
        $settings = (isset($this->default_fields["settings"]) && $this->default_fields["settings"]) ? true : false;

        acf_add_local_field_group($settings);

        foreach ($this->post_types as $post_type) {
            $location[] = array(array(
                "param" => "post_type",
                "operator" => "==",
                "value" => $post_type,
            ));
        }

        $this->builder_fields["location"] = $location;
       
        foreach ($this->blocks as $block) {
            /** @var \GF\Models\Component_PB_Block $block */

            /**
             * Don't add block if its not enabled on post type
             * 
             */
            if ($local_post_type !== null && \array_has_items($block->enabled_for_post_types) && !in_array($local_post_type, $block->enabled_for_post_types)) {
                continue;
            }

            $acffields = $block->get_pb_fields();

            $key = $block->get_pb_id();

            $fields = array();

            if ($block->settings == true) {
                
                $block_tab = $this->default_fields["block_tab"];
                $block_tab[0]["key"] = $key . "_" . $block_tab[0]["key"];

                $settings_tab = $this->default_fields["settings_tab"];
                $settings_tab[0]["key"] = $key . "_" . $settings_tab[0]["key"];

                $settings_fields = $this->clone_fields;
                $settings_fields[0]["key"] = $key . "_" . $settings_fields[0]["key"];

                $fields = array_merge($block_tab, $acffields, $settings_tab, $settings_fields);

            } else {
                $fields = $acffields;
            }


            // Check if this should be added to the dynamic views
            if(array_has_items($block->dynamic_pages)){

                foreach ($block->dynamic_pages as $page_name) {
                    // Create the array if not existant
                    if(!isset($this->dynamic_builder[$page_name])){
                        $this->dynamic_builder[$page_name] = array(
                            'key' => 'gf_dynamic_' . $page_name,
                            'title' => __('Build your dynamic content', 'graifkfabirken'),
                            'fields' => array(
                                array(
                                    'key' => 'gf_dynamic_page_builder_' . $page_name,
                                    'name' => 'gf_dynamic_page_builder_' . $page_name,
                                    'type' => 'flexible_content',
                                    'layouts' => array(),
                                    'button_label' => 'Lägg till rad',
                                    'min' => 0,
                                    'max' => 0,
                                )
                            )
                        );

                        // Fields
                        $this->dynamic_builder[$page_name]['location'] = array(array(array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'gf-pb-settings-page_builder_' . $page_name
                        )));

                    }

                    // Add this block to the page
                    $this->dynamic_builder[$page_name]['fields'][0]['layouts'][$block->get_pb_id()] = array(
                        'key' => $block->get_pb_id(),
                        'label' => $block->get_pb_name(),
                        'name' => $block->get_pb_id(),
                        'display' => 'block',
                        'sub_fields' => $block->get_pb_fields(),
                        'min' => 0,
                        'max' => 0
                    );

                    //Add screenshot
                    $this->add_screenshot($block->get_pb_id(), $block);
                }

            }

            if (!$block->dynamic_only) {
                $this->builder_fields["fields"][0]["layouts"][$key] = array(
                    'key' => $key,
                    'label' => $block->get_pb_name(),
                    'name' => $key,
                    'display' => 'block',
                    'sub_fields' => $fields,
                    'min' => "",
                    'max' => ""
                );

                //Add screenshot 
                $this->add_screenshot($key, $block);

            }

            /** @todo Check if this is needed */
            // foreach ($this->builder_fields_templates as $key1 => $value1) {
            //     $this->builder_fields_templates[$key1]["fields"][0]["layouts"][$key] = array(
            //         'key' => $key,
            //         'label' => $block->get_pb_name(),
            //         'name' => $key,
            //         'display' => 'block',
            //         'sub_fields' => $fields,
            //         'min' => 0,
            //         'max' => 0
            //     );
            // }

        }

        
        
        // Standard blocks
        acf_add_local_field_group($this->builder_fields);
        
        // Dynamic views
        if(array_has_items($this->dynamic_builder)){
            foreach ($this->dynamic_builder as $dynamic_builder_fields) {
                // dpr($dynamic_builder_fields,2);
                acf_add_local_field_group($dynamic_builder_fields);
            }
        }
    }

}