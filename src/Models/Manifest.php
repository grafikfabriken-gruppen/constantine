<?php
namespace GF\Models;


class Manifest{

    /**
     * Text domain
     *
     * @var strings
     */
    public $text_domain;

    /**
     * Companion functions
     *
     * @var array
     */
    public $companion;

    /**
     * Theme require this plugins
     *
     * @var array
     */
    public $plugins;
    
    /**
     * Theme is dependent on these scripts
     *
     * @var array
     */
    public $scripts;

    /**
     * Theme is dependent on these styles
     *
     * @var array
     */
    public $styles;

    /**
     * Available blocks
     *
     * @var array
     */
    public $components;

    /**
     * Loaded components
     * 
     * @var array
     */
    public $loaded_components = [];

    /**
     * Theme supports
     *
     * @var array
     */
    public $theme_supports;

    /**
     * Dynamic views
     *
     * @var array
     */
    public $dynamic_views;

    /**
     * Which post types should we use the pagebuilder on
     *
     * @var array
     */
    public $page_builder_for_post_types;

    /**
     * The base component
     *
     * @var Component
     */
    public $base_component;

    /**
     * The debug component
     *
     * @var Component
     */
    public $debug_component;

    /**
     * The header component
     *
     * @var Component
     */
    public $header_component;

    /**
     * The footer component
     *
     * @var Component
     */
    public $footer_component;

    /**
     * The Preloader Component
     *
     * @var Component
     */
    public $preloader_component;

    /**
     * The outdatedbrowser component
     *
     * @var Component
     */
    public $outdatedbrowser_component;

    /**
     * This is post is generated on the fly!
     *
     * @var array
     */
    public $auto_load = [];

    /**
     * Use theme builder?
     *
     * @var boolean
     */
    public $theme_builder;

    /**
     * Use generic blocks
     *
     * @var boolean
     */
    public $generic_blocks;

    /**
     * dataTypes for deserialation
     *
     * @var array
     */
    public $dataTypes = [
        "text_domain" => "string",
        "theme_builder" => "bool",
        "generic_blocks" => "bool",
        "companion" => "array",
        "plugins" => "array",
        "scripts" => "array",
        "styles" => "array",
        "components" => "array",
        "theme_supports" => "array",
        "dynamic_views" => "array",
        "page_builder_for_post_types" => "array"
    ];

    public function __construct()
    {
        
    }

    /**
     * Add auto load
     *
     * @param string $folder
     * @param string $name
     * @return void
     */
    public function add_auto_load($folder, $name){
        $data = array();
        $data[$folder] = $name;
        $add = true;

        if(array_has_items($this->auto_load)){
            foreach ($this->auto_load as $a) {
                if(isset($a[$folder]) && $a[$folder] == $name){
                    $add = false;
                }
            }
        }

        if($add){
            $this->auto_load = array_merge(array($data), $this->auto_load);
        }
    }

    /**
     * Save auto load file
     *
     * @return void
     */
    public function save_auto_load(){

        if(array_has_items($this->auto_load)){
            $data = \json_encode($this->auto_load);
            $file = \fopen(GF_THEME_AUTO_LOAD, 'w+');
            \fwrite($file, $data);
            \fclose($file);
        }

    }

    /**
     * Install all declared components in manifest
     *
     * @return void
     */
    public function install_components(){
        $this->load_components($this->components);
    }

    /**
     * Load an array of components
     *
     * @param array $components
     * @return void
     */
    public function load_components($components, $base_folder = null)
    {

        if (array_has_items($components)) {
            foreach ($components as $folder => $value) {
                if (is_string($value)) {
                    $this->install_component($folder, $value, $base_folder);
                } elseif (array_has_items($value)) {
                    if (isset($value["id"])) {
                        $this->install_component($folder, $value, $base_folder);
                    } else {
                        if(is_numeric($folder)){
                            $this->load_components($value, $base_folder);
                        }else{
                            $this->load_components($value, $folder);
                        }
                    }
                }
            }
        }

    }

    /**
     * Load component
     *
     * @param string $_folder
     * @param string $_array
     * @return Component|boolean
     */
    public function install_component($_folder, $_value, $_base_folder = null)
    {

        /**
         * Don't block blog id in options 
         * permalink screen
         * 
         * We need this for the autoload to generate complete!
         * 
         */
        $block_blog_id = true;
        if (isset($_SERVER["DOCUMENT_URI"]) && strpos($_SERVER["DOCUMENT_URI"], "options-permalink.php")) {
            $block_blog_id = false;
        }

        $blog_id = function_exists("get_current_blog_id") ? get_current_blog_id() : 0;

        $ovveride_data = [];
        $partials = [];
        $id = "";

        if(array_has_items($_value)){
            $id = $_value["id"];
            $ovveride_data = $_value;
            $partials = isset($_value["partials"]) ? $_value["partials"] : array();
            
            if(isset($_value["partials"])){
                unset($_value["partials"]);
            }
            
            unset($ovveride_data["id"]);

        }else{
            $id = $_value;
        }

        $info = $this->get_info($_folder, $id, $_base_folder);

        if (array_has_items($info)) {

            //Append som data!
            $info = array_merge($ovveride_data, $info);

            /** @var Component $model */
            $model = deserializeData($info, $info["model"]);

            if ($model != null) {

                /**
                 * Don't use this component 
                 * on all blogs except this!
                 * 
                 */
                if ($block_blog_id && $model->not_blog_id > 0) {
                    if ($blog_id == $model->not_blog_id) {
                        return false;
                    }
                }

                /**
                 * Don't use this component 
                 * on this site
                 * 
                 */
                if ($block_blog_id && $model->blog_id > 0) {
                    if ($blog_id != $model->blog_id) {
                        return false;
                    }
                }
                
                //If is a component
                if("Settings" ==  $model->get_type() && $loded_model = $this->maybe_get_component_by_id($model->id)){
                    return $loded_model;
                }

                //Not nice but nesscesary ovveride class
                if(array_has_items($partials)){
                    $model->ovveride_partials($partials);
                }

                //Add loaded component
                $this->loaded_components[] = $model;

                //Install sub components
                $model->install_sub_components();

                //Load acf fields
                $model->load_fields();

                if (array_has_items($model->sub_components)) {
                    // dpr($model, 2);
                }

                //Add controller to component
                $model->setup_controller();

                //Load acf hooks
                $model->theme_hooks();

                //Install to page builder
                $model->install_to_page_builder();


                //Hook up our base types!
                switch ($model->type) {
                    case 'Base':
                        $this->base_component = $model;
                        break;
                    case 'Globals_Footers':
                        $this->footer_component = $model;
                        break;
                    case 'Globals_Headers':
                        $this->header_component = $model;
                        break;
                    case 'Globals_Preloader':
                        $this->preloader_component = $model;
                        break;
                    case 'Globals_Outdatedbrowsers':
                        $this->outdatedbrowser_component = $model;
                        break;
					case 'Globals_Debug':
						$this->debug_component = $model;
						break;
                }

                

                return $model;

            }

        }

        return false;


    }


    /**
     * Get info about component
     *
     * @param string $folder
     * @param string $name
     * @return array
     */
    public function get_info($folder, $name, $base_folder = null){

        $folder = ucfirst($folder);
        $id = sprintf("%s_%s", $folder, $name);
        $model = sprintf("%s\%s", $folder, $name);
        $type = "";

        if ($base_folder !== null) {
            $base_folder = ucfirst($base_folder);
            $id = sprintf("%s_%s", $base_folder, $id);
            $model = sprintf("%s\%s", $base_folder, $model);
            $base_folder .= DIRECTORY_SEPARATOR;
        }

        $type = str_replace("_" . $name, '', $id);

        $model = sprintf("GF\Components\%s\Component", $model);

        $app_path = GF_THEME_COMPONENTS_DIR . DIRECTORY_SEPARATOR . $base_folder . $folder . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . "Component.php";
        $system_path = GF_COMPONENTS_DIR . DIRECTORY_SEPARATOR . $base_folder . $folder . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . "Component.php";
        $path = file_exists($app_path) == true ? $app_path : (file_exists($system_path) ? $system_path : false);

        if (!$path) {
            $error = new \WP_Error;
            $error->add('gf_could_not_find_component', "GF Constantine: Could not find component: {$base_folder}{$folder}/{$name}");
            $error->add('gf_could_not_find_component_paths', "Paths: {$app_path}<br>{$system_path}");
            wp_die($error);
            // return array();
        }

        $return = [
            "model" => $model,
            "id" => $id,
            "path" => dirname($path),
            "type" => $type
        ];

        return $return;

    }

    /**
     * Maybe get component by id
     *
     * @param string $id
     * @return Component|boolean
     */
    public function maybe_get_component_by_id($id){

        foreach($this->loaded_components as $component){
            if($component->id == $id) return $component;
        }

        return false;
    }

    /**
     * Get component by id
     *
     * @param string $id
     * @return Component
     */
    public function get_component_by_id($id){

        foreach($this->loaded_components as $component){
            if($component->id == $id) return $component;
        }

        throw new \Error("Could not find component with id: {$id}");
    }

    /**
     * Get first component by type
     *
     * @param string $type
     * @return Component
     */
    public function get_component_by_type($type)
    {
        foreach ($this->loaded_components as $component) {
            if ($component->type == $type){

                return $component;
            } 
        }

        throw new \Error("Could not find component with type: {$type}");
    }

}