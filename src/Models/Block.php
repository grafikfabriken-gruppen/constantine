<?php
namespace GF\Models;

class Block extends \Custom_Cpt_Model
{

    /**
     * @var $post_type type of the block
     */
    public $post_type = 'block';

    /**
     * Get the title of the post.
     *
     * @return string
     */
    public function get_title()
    {
        return apply_filters('block_title', $this->post ? $this->post->post_title : '', $this);
    }

}