<?php

namespace GF\Models;

use function GF\Utils\renderBlock;
use function GF\Utils\fieldsFactory;
use function GF\Utils\builderBlockFactory;


class Component_PB_Block extends Component
{

    /**
     * Html id
     *
     * @var string
     */
    public $html_id;

    /**
     * Acf layout
     *
     * @var string
     */
    public $acf_fc_layout;

    /**
     * Use a custom wrapper,
     * This only appends to pagebuilder components
     *
     * @var boolean
     */
    public $custom_wrapper = false;


    /**
     * Should this section count towards the snap-factory-index?
     * This is useful for when you're rendering other sections inside this section
     *
     * @var bool
     */
    public $use_section_index = true;

    /**
     * Css class
     *
     * @var string
     */
    public $css;

    /**
     * Full width?
     *
     * @var boolean
     */
    public $full_width = false;

    /**
     * Full width content?
     *
     * @var boolean
     */
    public $full_width_content = false;

    /**
     * Allow behind header
     *
     * @var boolean
     */
    public $allow_behind_header = false;

    /**
     * Full height?
     *
     * @var boolean
     */
    public $full_height = false;

    /**
     * Block background color
     *
     * @var string
     */
    public $background_color;

    /**
     * Block background image
     *
     * @var string
     */
    public $background_image;

    /**
     * Use settings in pagebuilder
     *
     * @var boolean
     */
    public $settings = false;

    /**
     * Should the builder block only be attached to the dynamic builder pages
     *
     * @var bool
     */
    public $dynamic_only = false;

    /**
     * Which dynamic pages should this appear on?
     *
     * @var array[string]
     */
    public $dynamic_pages = array();

    /**
     * Is component snappable?
     *
     * @var boolean
     */
    public $snap = false;

    /**
     * Allows next section to snap to this section
     *
     * @var boolean
     */
    public $snappable = false;

    /**
     * Is component snapped?
     *
     * @var boolean
     */
    public $snapped = false;

    /**
     * Should this (only extending component) use the parent template
     *
     * @var bool
     */
    public $use_parent_template = false;

    /**
     * Is block only enabled for post types
     * 
     * @var array[string]
     */
    public $enabled_for_post_types = [];

    public function __construct()
    {

        //Extend dataTypes
        $this->dataTypes = array_merge($this->dataTypes, array(
            "css" => "string",
            "html_id" => "string",
            "background_color" => "string",
            "background_image" => "string",
            "custom_wrapper" => "bool",
            "full_width" => "bool",
            "full_width_content" => "bool",
            "full_height" => "bool",
            "allow_behind_header" => "bool",
            "settings" => "bool",
            "acf_fc_layout" => "string",
            "dynamic_pages" => "array",
            "dynamic_only" => "bool"
        ));
    }

    /**
     * Render page builder block
     *
     * @return string
     */
    public function render()
    {

        ob_start();
        
        if ($this->custom_wrapper === true) {
            include $this->get_template_path();
        } else {
            include GF_TEMPLATES_WRAPPERS . DIRECTORY_SEPARATOR . 'start.php';
            include $this->get_template_path();
            include GF_TEMPLATES_WRAPPERS . DIRECTORY_SEPARATOR . 'end.php';
        }
        
        return ob_get_clean();
        
    }


    /**
     * Get pagebuilder ID
     *
     * @return string
     */
    public function get_pb_id()
    {
        return strtolower('builder_block_' . strtolower($this->id));
    }

    /**
     * Get page builder name
     *
     * @return string
     */
    public function get_pb_name()
    {
        return $this->pretty_name ? $this->pretty_name : $this->get_pb_id();
    }

    /**
     * Install component to page builder
     *
     * @return void
     */
    public function install_to_page_builder()
    {
        builderBlockFactory()->add_block($this->get_pb_id(), $this);
    }

    /**
     * Get screenshot
     *
     * @return string
     */
    public function get_screenshot()
    {

        $location = $this->getDir() . "/screenshot.jpg";


        if (file_exists($location) && defined('WP_CONTENT_URL')) {
            $url = WP_CONTENT_URL;
            $image = preg_replace('/.*(\/themes\/[a-zA-Z]*)/', $url . '$1', $location);
            return $image;
        }

        return '';
    }

    /**
     * Get pagebuilder fields
     *
     * @return void
     */
    public function get_pb_fields()
    {
        return array_has_items($this->fields) ? $this->fields : fieldsFactory()->buildBlockFields($this->get_pb_name(), $this->get_pb_id(), $this->fields);
    }

    /**
     * Get section css class
     *
     * @return string
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    public function get_css_class()
    {

        if ($this->snapped) {
            $this->css = "snapped " . $this->css;
        }

        if ($this->snappable) {
            $this->css = "snappable " . $this->css;
        }

        if ($this->allow_behind_header) {
            $this->css = "behind_header " . $this->css;
        }

        return sprintf(
            "gf%s%s",
            $this->css ? ' ' . $this->css : '',
            ' ' . $this->acf_fc_layout
        );
    }



    /**
     * Get class for row
     *
     * @return string
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    public function get_container_class()
    {
        return sprintf(
            "container%s%s",
            $this->full_width_content ? "-fluid" : "",
            $this->full_height ? " full_height" : ""
        );
    }

    /**
     * Get background image
     *
     * @return string
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    public function get_bg($type = 'container')
    {

        if ($type === 'container' && $this->full_width) {
            return;
        } elseif ($type === 'section' && !$this->full_width) {
            return;
        }

        $style = '';

        if (isset($this->background_image) && $this->background_image !== null) {
            if (strpos($this->background_image, 'http') === 0) {
                $style = 'background-image:url(' . $this->background_image . ');';
            } elseif ($bg = wp_get_attachment_image_src($this->background_image, "full")) {
                $style = 'background-image:url(' . aq_resize($bg[0], 1200) . ');';
            }
        }

        if (isset($this->background_color) && $this->background_color !== null) {
            $style .= "background-color:$this->background_color;";
        }

        return $style === '' ? "" : 'style="' . $style . '"';
    }

    /**
     * Get id for html
     *
     * @return string
     */
    public function get_html_id()
    {
        return $this->html_id ? sprintf('id="%s"', $this->html_id) : '';
    }
}
