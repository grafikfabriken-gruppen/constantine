<?php
namespace GF\Models;

use GF\Interfaces\Interface_Component;
use function GF\Utils\fieldsFactory;
use function GF\Utils\builderBlockFactory;
use function GF\Utils\pageBuilder;
use function GF\Utils\renderBlock;
use function GF\Utils\manifestController;
use function GF\Utils\templateController;

abstract class Component implements Interface_Component{

    /**
     * Component id
     *
     * @var string
     */
    public $id;

    /**
     * Pretty name
     *
     * @var string
     */
    public $pretty_name;

    /**
     * Template name
     *
     * @var string
     */
    public $template_name = "template";

    /**
     * Which file contains our ACF Fields
     *
     * @var string
     */
    public $field_file_name = "fields.php";
    
    /**
     * Which file contains our ACF Fields for options
     *
     * @var string
     */
    public $option_field_file_name = "option_fields.php";
    
    /**
     * Component type
     *
     * @var string
     */
    public $type;

    /**
     * Base path to component
     *
     * @var string
     */
    public $path;

    /**
     * Acf fields
     *
     * @var array
     */
    public $fields;

    /**
     * Acf fields for options
     *
     * @var array
     */
    public $option_fields;

    /**
     * Has component options?
     * Use this on your fields AND
     * add class_key=class_property 
     * to auto load options
     *
     * @var boolean
     */
    public $has_options = false;

    /**
     * Has component loaded the options
     *
     * @var boolean
     */
    public $loaded_options = false;

    /**
     * Partials
     *
     * @var array
     */
    public $partials;

    /**
     * Sub components
     *
     * @var array[Component]
     */
    public $sub_components;

    /**
     * Enable in page builder
     *
     * @var boolean
     */
    public $enable_in_page_builder = false;


    /**
     * If component use controller assign this with namespace
     *
     * @var string
     */
    public $controller_name = '';

    public $internal_section_name = '';

    public $title_tag = 'h3';
    public $sub_title_tag = 'h3';
    public $left_title_tag = 'h3';
    public $right_title_tag = 'h3';

    /**
     * An actual controller
     *
     * @var Mixed
     */
    public $controller;

    /**
     * This component only using this variables
     *
     * @var array
     */
    public $options_in_use = array();

    /**
     * Should the builder block only be attached to the dynamic builder pages
     *
     * @var bool
     */
    public $dynamic_only = false;

     /**
     * Which dynamic pages should this appear on?
     *
     * @var array[string]
     */
    public $dynamic_pages = array();

    /**
     * Is this component dynamic?
     *
     * @var bool
     */
    public $is_dynamic = false;

    /**
     * Is Component active for blog_id?
     * 
     * 0 = ALL
     *
     * @var integer
     */
    public $blog_id = 0;

    /**
     * Is Component active for blog_id?
     * 
     * 0 = ALL
     *
     * @var integer
     */
    public $not_blog_id = 0;
  
    /**
     * Datatypes for deserialation
     *
     * @var array
     */
    public $dataTypes = [
        "dataTypes" => "array",
        "id" => "string",
        "type" => "string",
        "path" => "string",
        "pretty_name" => "string",
        "template_name" => "string",
        "css" => "string",
        "background_color" => "string",
        "background_image" => "string",
        "custom_wrapper" => "bool",
        "full_width" => "bool",
        "full_width_content" => "bool",
        "cache" => "bool",
        "full_height" => "bool",
        "enable_in_page_builder" => "bool",
        "acf_fc_layout" => "string",
        "partials" => "array",
        "sub_components" => "array",
        "controller_name" => "string",
        "dynamic_pages" => "array",
        "dynamic_only" => "bool",
        "blog_id" => "int",
        "not_blog_id" => "int"
    ];

    /**
     * Can we cache this component?
     *
     * @var boolean
     */
    public $cache = false;


   

    public function __construct(){

    }

    /**
     * Get title tag
     *
     * @param string $key
     * @param string $tag
     * @param array $additional_css_classes
     * 
     * @return string
     * 
     */
    public function get_title_tag($key, $default_title_tag = 'h2', $additional_css_classes = []): string
    {

        /**
         * Property dosen't exist
         * 
         */
        if (!property_exists($this, $key)) {
            return '';
        }

        $title = $this->{$key};

        if ($title == "") return "";

        $tag_key = $key . "_tag";
        $ovveride_title_tag = $default_title_tag;

        if (property_exists($this, $tag_key) && $this->{$tag_key} != 'default') {
            $ovveride_title_tag = $this->{$tag_key};
        }

        if ($default_title_tag != $ovveride_title_tag) {
            $additional_css_classes[] = sprintf("%s", $default_title_tag);
        }

        $css_class = implode(' ', $additional_css_classes);

        return sprintf('<%s%s>%s</%s>', $ovveride_title_tag, $css_class !== "" ? " class=\"{$css_class}\"" : "", $title, $ovveride_title_tag);
    }

    public function is_dynamic(){
        return $this->is_dynamic;
    }

    /**
     * If we have an controller name try to setup controller
     *
     * @return void
     */
    public function setup_controller(){
        if(class_exists($this->controller_name)){
            
            try {
                $this->controller = call_user_func_array(array($this->controller_name, 'getInstance'), array());
            
            } catch(\Exception $e) {

                die($this->controller_name . ' not found');
            }
        }

    }

    /**
     * Render component
     *
     * @return string
     */
    public function render()
    {
        $this->set_options();

        $path = $this->get_template_path();

        if($path === '') return;

        ob_start();
            include $this->get_template_path();
        return ob_get_clean();

    }

    /**
     * For dynamic blocks this is wehere you'd want to take the queried object and extract the data to the component
     *
     * @param mixed $object
     * @return void
     */
    public function map_dynamic_data($object){}


    /**
     * This runs when component is installed
     *
     * @return void
     */
    public function theme_hooks(){}

    /**
     * This runs before block is displayed!
     *
     * @return void
     */
    public function before_render(){}

    /**
     * Load acf fields
     *
     * @return void
     */
    public function load_fields(){

        $path = sprintf("%s/%s", $this->path, $this->field_file_name);
        
        if(file_exists($path)){
            $this->fields = include $path;
        }
        
        $option_path = sprintf("%s/%s", $this->path, $this->option_field_file_name);
        
        if(file_exists($option_path)){
            $this->has_options = true;
            $this->option_fields = include $option_path;
        }

    }

    /**
     * Get aq image url from a object key
     *
     * @param string $key
     * @param int $width
     * @param int|bool $height
     * @param boolean $crop
     * @param boolean $single
     * @param boolean $upscale
     * @return string
     */
    public function get_aq_image_url($key, $width, $height = null, $crop = true, $single=true, $upscale=true){

        $image_id  = (int) $key > 0 ? $key : 0;

        $url = wp_get_attachment_image_url($image_id, 'full');

        if($url){
            return aq_resize($url, $width, $height, $crop, $single, $upscale);
        }

        return '';

    }

    /**
     * Get sub components fields
     *
     * @param string $key
     * @param string $acf_key
     * @param boolean $singular is this a singular sub component field?
     * @return array
     */
    public function get_sub_component_fields($key, $singular = false){

        if(array_has_items($this->sub_components) && isset($this->sub_components[$key]) && array_has_items($this->sub_components[$key]->fields)){

            $fields = $this->sub_components[$key]->fields;

            if ($singular) {

                $acf_key = strtolower($this->id) . $key;
                
                $fields = [
                    'key' => $acf_key,
                    'label' => $this->sub_components[$key]->pretty_name,
                    'name' => $key,
                    'type' => 'group',
                    'layout' => 'block',
                    'sub_fields' => $fields
                ];

            }

            //This line is nescessary for sub components to ovveride components!!
            if(isset($fields["name"]) && isset($fields["type"]) && $fields["type"] === "group"){
                // $fields["name"] = $key;
                // $fields["key"] = $key;
                $this->sub_components[$key]->fields = $fields;
            }

            return $fields;
        }

        return array();
    }

    /**
     * Used to be overwritten. Can be used for template manipulation
     *
     * @return string
     */
    public function get_component_dir(){
        return __DIR__;
    }

    /**
     * Installs sub components
     *
     * @param Array $data
     * @return void
     */

    public function install_sub_components()
    {

        if (array_has_items($this->partials)) {

            foreach ($this->partials as $class_key => $partial) {

                if (array_has_items($partial)) {

                    $folder = isset($partial["folder"]) ? $partial["folder"] : null;

                    $id = isset($partial["id"]) ? $partial["id"] : null;

                    $is_array = isset($partial["type"]) && $partial["type"] == "array";

                    $data = [
                        "id" => $id
                    ];

                    if ($folder !== null) {
                        unset($partial["folder"]);
                    }

                    $data = array_merge($partial);

                    if ($folder !== null && $id !== null) {

                        if ($model = manifestController()->manifest->install_component($folder, $data, 'partials')) {

                            //Auto load this sub component
                            manifestController()->manifest->add_auto_load($folder, $data["id"]);

                            $dataType = isset($this->dataTypes[$class_key]) ? $this->dataTypes[$class_key] : null;
                            $class_name = get_class($model);

                            if ($dataType && strpos($dataType, 'array[') !== false || $is_array) {
                                $this->dataTypes[$class_key] = "array[{$class_name}]";
                            } else {
                                $this->dataTypes[$class_key] = "{$class_name}";
                            }

                            $this->sub_components[$class_key] = $model;

                            if (array_has_items($model->partials)) {
                                $model->install_sub_components();
                            }
                        }
                    }
                }
            }
        }
    }

    public function ovveride_partials($data){
        if(array_has_items($data)){
            foreach ($data as $key => $value) {
                if (array_has_items($value)) {
                    if (array_has_items($this->{$key})) {
                        $this->{$key} = array_merge($this->{$key}, $value);
                    }
                } else {
                    $this->{$key} = $value;
                }
            }
        }
    }

    public function get_type(){
        return $this->type;
    }

    /**
     * This runs when all components is installed!
     *
     * @return void
     */
    public function installed(){}

    /**
     * Use only theese options
     *
     * @param [type] $args
     * @return void
     */
    public function use_options($args){
        $this->options_in_use = array_merge($this->options_in_use, $args);
    }

    /**
     * Install component to page builder
     *
     * @return void
     */
    public function install_to_page_builder(){}

    /**
     * Get screenshot
     *
     * @return string
     */
    public function get_screenshot(){ return ''; }

    /**
     * Set ACF options to the model
     *
     * @return void
     */
    public function set_options(){
        
        if ($this->has_options && !$this->loaded_options) {
            
            if (array_has_items($this->options_in_use) && array_has_items($this->option_fields)) {
                /** @todo don't use options fields!!! */
                foreach ($this->option_fields as $field) {
                    if (
                        (isset($field["class_key"]) && in_array($field["class_key"], $this->options_in_use )) && 
                        property_exists($this, $field["class_key"])) {
                        // What should the name on the class be. To avoid overrites since it's the wp_options table
                        $this->{$field["class_key"]} = get_field($field["name"], 'option');
                    }
                }
            
            }

            $this->loaded_options = true;
        }
    }

    /**
     * Get real template path for component
     *
     * @return string
     */
    public function get_template_path(){

        if(!$this->path) $this->path = $this->getDir();

        $path_php = sprintf("%s/%s.php", $this->path, $this->template_name);
        $path_html = sprintf("%s/%s.html", $this->path, $this->template_name);

        
        $template = file_exists($path_php) ? $path_php : (file_exists($path_html) ? $path_html : '');
        return $template;

    }

    /**
     * Can be used if the component needs some options before the time of rendering
     *
     * @return void
     */
    public function force_load_options(){
        templateController()->install_global_partials($this);
    }

    protected function getDir()
    {
        $reflector = new \ReflectionClass(get_class($this));
        return dirname($reflector->getFileName());
    }

    /**
     * Get template as string
     *
     * @return string
     */
    public function get_template(){
        ob_start();
            include $this->get_template_path();
        return ob_get_clean();

    }


    public function compiled_by_pagebuilder(){

    }

    /**
     * Set templates and id
     *
     * @return void
     */
    public function setinfo(){

        $folder = ucfirst($folder);
        $id = sprintf("%s_%s", $folder, $name);
        $model = sprintf("%s\%s", $folder, $name);
        $type = "";

        if ($base_folder !== null) {
            $base_folder = ucfirst($base_folder);
            $id = sprintf("%s_%s", $base_folder, $id);
            $model = sprintf("%s\%s", $base_folder, $model);
            $base_folder .= DIRECTORY_SEPARATOR;
        }

        $type = rtrim($id, "_" . $name);
        $model = sprintf("GF\Components\%s\Component", $model);

        $app_path = GF_THEME_COMPONENTS_DIR . DIRECTORY_SEPARATOR . $base_folder . $folder . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . "Component.php";
        $system_path = GF_COMPONENTS_DIR . DIRECTORY_SEPARATOR . $base_folder . $folder . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . "Component.php";
        $path = file_exists($app_path) == true ? $app_path : (file_exists($system_path) ? $system_path : false);

        if (!$path) {
            $error = new \WP_Error('gf_could_not_find_component', "GF Constantine: Could not find component: {$base_folder}{$folder}/{$name}");
            // wp_die($error);
            return array();
        }

        $return = [
            "model" => $model,
            "id" => $id,
            "path" => dirname($path),
            "type" => $type
        ];

        return $return;

    }

}