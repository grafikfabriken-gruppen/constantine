<?php
namespace GF\Controllers;
use GF\Utils\Singleton;
use GF\Post_types\Post\Post_Controller;
use GF\Post_types\Page\Page_Controller;
use GF\Post_types\Taxonomy\Category\Category_Controller;

class WP_Controller extends Singleton{

    /**
     * Single Post
     *
     * @var \Custom_Cpt_Model
     */
    public $queryied_post;

    /**
     * Post controller
     *
     * @var \GF\Post_types\Post\Post_Controller
     */
    public $post_controller;

    /**
     * Category controller
     *
     * @var \GF\Post_types\Taxonomy\Category\Category_Controller
     */
    public $category_controller;

    /**
     * Page controller
     *
     * @var \GF\Post_types\Page\Page_Controller
     */
    public $page_controller;

    public function _construct(){
        
        $this->post_controller = Post_Controller::getInstance();
        $this->page_controller = Page_Controller::getInstance();
        $this->category_controller = Category_Controller::getInstance();

    }

    /**
     * Get single post as a gf model
     *
     * @return \Custom_Cpt_Model
     */
    public function get_queryied_post_object(){

        if(is_a($this->queryied_post, '\Custom_Cpt_Model')){
            return $this->queryied_post;
        }

        global $post;

        if(is_a($post, 'WP_Post')){
            $this->queryied_post = \get_custom_model($post);
            return $this->queryied_post;
        }

        return false;

    }
    
}