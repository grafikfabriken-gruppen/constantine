<?php
namespace GF\Controllers;

use GF\Utils\Singleton;
use function GF\Utils\pageBuilder;

class Block_Controller extends Singleton
{

    /**
     * Constructing 
     *
     * @return void
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    public function _construct()
    {
        // Register post type
        \add_cpt('\GF\Models\Block', array(
            'name' => 'block',
            'singular' => __('General block', "grafikfabriken"),
            'plural' => __('General blocks', "grafikfabriken"),
            'args' => array(
                'has_archive' => false,
                'hierarchical' => true,
                'query_var' => false,
                'publicly_queryable' => false,
                'supports' => array('title'),
                'menu_icon' => 'dashicons-editor-table'
            )
        ));

        if (\is_admin()) {

            \add_action('admin_init', function () {
                \GF_Helper::add_column('block', __("Shortcode", 'grafikfabriken'), 'block', array($this, "add_shortcode_col"), 2);
            });

            \GF_Helper::add_meta_box(array('block'), 'gf_block-results', __('Shortcode', 'grafikfabriken'), array($this, "display_meta_box"), 'side', 'high');
        }

        \add_shortcode("gf_block", array($this, "render_block"));

    }

    /**
     * Display meta box
     *
     * @param \WP_Post $post
     * @return void
     */
    public function display_meta_box($post)
    {
        echo '[gf_block title="' . $post->post_title . '" id="' . $post->ID . '"]';
    }

    /**
     * public function render_shortcode($args, $content = null)
     * @param array $args
     * @param string $content
     * @return string
     * 
     * Note: How to use:
     * Shortcode [gf_block id="WP_POST->ID"]
     */
    public function render_block($args, $content = null)
    {

        $args = \shortcode_atts(array(
            'id' => 0,
            'class' => 'module',
        ), $args);

        $config = $args;

        if (is_numeric($config["id"]) && $post = get_post($config["id"])) {
            $block = \get_custom_model($post);
            if ($block->post_type != "block")
                return false;

            return pageBuilder()->render(false, $block->id);
        }
    }

    /**
     * Add shortcode col
     *
     * @param int $post_id
     * @return void
     */
    public function add_shortcode_col($post_id)
    {
        $block = \get_custom_model($post_id);
        echo '[gf_block title="' . $block->get_title() . '" id="' . $block->id . '"]';
    }


}