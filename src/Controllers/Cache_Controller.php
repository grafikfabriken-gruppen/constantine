<?php

namespace GF\Controllers;

use GF\Utils\Singleton;
use League\Uri\QueryString;
use League\Uri\Uri;

use function GF\Utils\optionsPageFactory;

class Cache_Controller extends Singleton
{

    /**
     * Cahce file ending
     *
     * @var string
     */
    public $fileEnding = '.html';

    /**
     * Should we use memcache?
     *
     * @var boolean
     */
    public $use_memcache = false;

    /**
     * Memcache object
     *
     * @var \Memcached
     */
    public $memcache;

    /**
     * Uri
     *
     * @var string
     */
    public $uri;

    /**
     * Cahce querystrings
     *
     * @var array
     */
    public $cache_querystrings = ["p", "s", "paged", "page"];

    public function _construct()
    {

        if (class_exists('\Memcached')) {
            $this->use_memcache = true;
            $this->memcache = new \Memcached();

            $server = defined('MEMCACHE_SERVER') ? MEMCACHE_SERVER : '127.0.0.1';
            $port = defined('MEMCACHE_PORT') ? MEMCACHE_PORT : 11211;

            $this->memcache->addServer($server, $port);
            $statuses = $this->memcache->getStats();
            if ($statuses[$server . ':' . $port]['uptime'] < 1) {
                $this->use_memcache = false;
            } else {
                $this->use_memcache = true;
            }
        }

        //Delete cache on save post
        add_action('save_post', [$this, 'delete_all_cache'], 10, 1);

        //Clear cache on acf save post
        add_action('acf/save_post', [$this, 'acf_save_post']);

        // Add clear cache button
        add_action('admin_bar_menu', [$this, 'add_clear_cache_btn'], 999);

        // Shall we clear the cache?
        add_action('admin_init', [$this, 'maybe_clear_cache']);
        add_action('admin_init', [$this, 'maybe_show_clear_cache_notice']);

        // Only set uri in frontend
        add_action('wp', [$this, 'set_uri']);
        
    }

    /**
     * Set URI
     *
     * @return void
     */
    public function set_uri()
    {

        try {
            $uri = Uri::createFromServer($_SERVER);

            $cache_querystring = [];
            $_parsed_querystring = QueryString::parse($uri->getQuery());

            $this->cache_querystrings = apply_filters('constantine_cache_querystrings', $this->cache_querystrings);

            if (array_has_items($_parsed_querystring)) {
                foreach ($_parsed_querystring as $query) {
                    if (array_has_items($query) && isset($query[0]) && in_array($query[0], $this->cache_querystrings)) {
                        $cache_querystring[] = $query;
                    }
                }
            }

            $new_query_string = QueryString::build($cache_querystring);
            $uri = $uri->withQuery($new_query_string);
            $this->uri = $uri->__toString();
        } catch (\Exception $e) {
            $this->uri = "";
        }

            

    }

    /**
     * Maybe clear cache
     *
     * @return void
     */
    public function maybe_clear_cache(): void
    {
        if (isset($_GET['gf-constantine-clear-cache']) && $_GET['gf-constantine-clear-cache'] == 'true') {
            $this->delete_all_cache();

            if (strpos($_SERVER["HTTP_REFERER"], admin_url()) === 0) {
                $redirect = add_query_arg(array('clear-cache-notice' => 'true', 'gf-constantine-clear-cache' => 'done'), $_SERVER["HTTP_REFERER"]);
            } else {
                // Redirect back
                $redirect = $_SERVER["HTTP_REFERER"];
            }

            // If WP-rocket is there we'll just empty their cache aswell
            if (function_exists('rocket_clean_domain')) {
                rocket_clean_domain();
            }

            \wp_safe_redirect($redirect);
            die;
        }
    }

    /**
     * Maybe show notice
     *
     * @return void
     */
    public function maybe_show_clear_cache_notice(): void
    {
        if (isset($_GET['clear-cache-notice']) && $_GET['clear-cache-notice'] == 'true' && isset($_GET['gf-constantine-clear-cache'])) {
            \GF_Helper::show_notice(__('Theme cache has been cleared', 'grafikfabriken'));
        }
    }

    /**
     * Adds a button for clearing the theme cache
     * 
     * @return void
     */
    public function add_clear_cache_btn($wp_admin_bar): void
    {

        if (defined('DISABLE_CACHE') && DISABLE_CACHE) return;
        if (function_exists('rocket_clean_domain')) return;

        $args = array(
            'id'    => 'constantine_clear_cache',
            'title' => __('Clear cache', 'grafikfabriken'),
            'href'  => admin_url('?gf-constantine-clear-cache=true'),
            // 'meta'  => array('class' => 'js-constantine-clear-cache')
        );

        $wp_admin_bar->add_node($args);
        
    }

    /**
     * ACF Save post hook
     *
     * @return void
     */
    public function acf_save_post(): void
    {

        $screen = get_current_screen();
        $pages = optionsPageFactory()->option_pages;

        if (is_a($screen, '\WP_Screen') && array_has_items($pages)) {

            foreach ($pages as $key => $value) {
                if (strpos($screen->id, $value) == true) {
                    $this->delete_all_cache();
                    break;
                }
            }
        }
    }

    /**
     * Delete cache
     *
     * @param int $id
     * @return void
     */
    public function delete_all_cache($dir = '')
    {
        if ($this->use_memcache) {
            $this->memcache->flush();
            return;
        }

        if (!$dir) $dir = GF_THEME_CACHE_FOLDER;



        if (is_dir($dir)) {

            $objects = scandir($dir);



            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object)) {
                        $this->delete_all_cache($dir . DIRECTORY_SEPARATOR . $object);
                    } else {
                        unlink($dir . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
            rmdir($dir);
        }
    }
    /**
     * Delete cache
     *
     * @param string $hash
     * @return void
     */
    public function delete_cache($hash)
    {

        if ($this->use_memcache) {
            $this->memcache->flush();
        } else {

            $folder = GF_THEME_CACHE_FOLDER . DIRECTORY_SEPARATOR . $hash;
            if (is_dir($folder)) {
                array_map('unlink', glob("$folder/*.*"));
                rmdir($folder);
            }
        }
    }

    /**
     * Save cache!
     *
     * @param string $hash
     * @param string $html
     * @return void
     */
    public function save_cache($hash, $html)
    {

        // No matter if this file exists, dont' save it
        if (defined('DISABLE_CACHE') && DISABLE_CACHE) return;

        // Don't cache logged in users
        if (is_user_logged_in()) return false;

        $root_folder = GF_THEME_CACHE_FOLDER;
        $cache_folder = $root_folder . DIRECTORY_SEPARATOR . $hash;
        $file_name = $this->get_cache_file_name($hash);

        if ($this->use_memcache) {
            $this->memcache->set($hash, $this->minify($html));
        } else {

            if (!file_exists($root_folder)) {
                mkdir($root_folder);
            }

            if (!file_exists($cache_folder)) {
                mkdir($cache_folder);
            }

            if (file_exists($cache_folder)) {
                file_put_contents($this->get_cache_file_name($hash), $this->minify($html));
            }
        }
    }

    /**
     * Minify Html
     *
     * @param string $body
     * @return string
     */
    public function minify(String $body): string
    {

        //remove redundant (white-space) characters
        $replace = array(
            //remove tabs before and after HTML tags
            '/\>[^\S ]+/s' => '>',
            '/[^\S ]+\</s' => '<', //shorten multiple whitespace sequences; keep new-line characters because they matter in JS!!!
            '/([\t ])+/s' => ' ', //remove leading and trailing spaces
            '/^([\t ])+/m' => '',
            '/([\t ])+$/m' => '', // remove JS line comments (simple only); do NOT remove lines containing URL (e.g. 'src="http://server.com/"')!!!
            '~//[a-zA-Z0-9 ]+$~m' => '', //remove empty lines (sequence of line-end and white-space characters)
            '/[\r\n]+([\t ]?[\r\n]+)+/s' => "\n", //remove empty lines (between HTML tags); cannot remove just any line-end characters because in inline JS they can matter!
            '/\>[\r\n\t ]+\</s' => '><', //remove "empty" lines containing only JS's block end character; join with next line (e.g. "}\n}\n</script>" --> "}}</script>"
            '/}[\r\n\t ]+/s' => '}',
            '/}[\r\n\t ]+,[\r\n\t ]+/s' => '},', //remove new-line after JS's function or condition start; join with next line
            '/\)[\r\n\t ]?{[\r\n\t ]+/s' => '){',
            '/,[\r\n\t ]?{[\r\n\t ]+/s' => ',{', //remove new-line after JS's line end (only most obvious and safe cases)
            '/\),[\r\n\t ]+/s' => '),', //remove quotes from HTML attributes that does not contain spaces; keep quotes around URLs!
            '~([\r\n\t ])?([a-zA-Z0-9]+)="([a-zA-Z0-9_/\\-]+)"([\r\n\t ])?~s' => '$1$2=$3$4', //$1 and $4 insert first white-space character found before/after attribute
        );
        $body = preg_replace(array_keys($replace), array_values($replace), $body);

        //remove optional ending tags (see http://www.w3.org/TR/html5/syntax.html#syntax-tag-omission )
        $remove = array(
            '</option>', '</li>', '</dt>', '</dd>', '</tr>', '</th>', '</td>'
        );

        $body = str_ireplace($remove, '', $body);

        return $body;
    }

    /**
     * Get cache file name
     *
     * @param string $id
     * @param string $hash
     * @return string
     */
    public function get_cache_file_name($hash): string
    {
        return GF_THEME_CACHE_FOLDER . DIRECTORY_SEPARATOR . $hash . DIRECTORY_SEPARATOR . $hash . $this->fileEnding;
    }

    /**
     * Does cache exist?
     *
     * @param string $id
     * @param string $hash
     * @return boolean
     */
    public function exists(String $hash): bool
    {
        // No matter if this file exists, dont' serve it
        if (defined('DISABLE_CACHE') && DISABLE_CACHE) return false;

        // Don't cache logged in users
        if (is_user_logged_in()) return false;

        if ($this->use_memcache) {
            return $this->memcache->get($hash) ? true : false;
        } else {
            return file_exists($this->get_cache_file_name($hash));
        }
    }

    /**
     * Get cache content
     *
     * @param string $id
     * @param string $hash
     * @return string
     */
    public function get_cache(String $hash): string
    {

        $content = '';

        if ($this->exists($hash)) {
            ob_start();

            ?>
            <!-- GF CACHE: <?= $hash ?> -->
            <?php
            if ($this->use_memcache) {
                echo $this->memcache->get($hash);
            } else {
                include $this->get_cache_file_name($hash);
            }

            $content = ob_get_clean();
        }

        return $content;
    }

    /**
     * Generate hash
     *
     * @param string $data
     * @return string
     */
    public function generate_hash(String $data): string
    {
        return sanitize_title($this->uri) . "_" . $data;
    }
}
