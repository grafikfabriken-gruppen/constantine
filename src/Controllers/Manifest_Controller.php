<?php

namespace GF\Controllers;

use GF\Models\Manifest;
use GF\Utils\Singleton;
use Symfony\Component\Yaml\Yaml;

class Manifest_Controller extends Singleton
{

    /**
     * Manifest
     *
     * @var Manifest
     */
    public $manifest;

    /**
     * All the loaded components
     *
     * @var array
     */
    public $components;

    /**
     * Setup manifest
     *
     * @return Manifest
     */
    public function setup()
    {

        try {
            $manifest = Yaml::parseFile(GF_THEME_MANIFEST);
            $this->manifest = deserializeData($manifest, 'GF\Models\Manifest');
            // dpr($this->manifest,2);
            return $this->manifest;
        } catch (\Exception $e) {
            $error = new \WP_Error('gf_could_not_parse_manifest', 'Could not parse manifest!');
            wp_die($error);
        }
    }
}
