<?php

namespace GF\Controllers;

use GF\Controllers\Manifest_Controller;
use GF\Models\Manifest;
use GF\Utils\String_Utils;
use GF\Utils\Singleton;
use GF\Themebuilder\Utils;
use function GF\Utils\templateController;
use function GF\Utils\templateWrapController;
use function GF\Utils\manifestController;
use function GF\Utils\pageBuilder;
use function GF\Utils\builderBlockFactory;
use function GF\Utils\fieldsFactory;
use function GF\Utils\blockController;
use function GF\Utils\optionsPageFactory;
use function GF\Themebuilder\Utils\bootstrapThemebuilder;
use function GF\Utils\cacheController;

/**
 * Bootstrap_Controller controller
 * 
 * @namespace GF\Controllers
 * @author Hampus Alstermo <hampus@grafikfabriken.nu>
 * @version 0.1
 */
class Bootstrap_Controller extends Singleton
{

    /**
     * Theme manifest
     *
     * @var Manifest 
     */
    private $manifest;

    /**
     * Bootstrap_Controller
     *
     * @return void
     */
    public function _construct()
    {

        //Setup defines
        $this->setupDefines();

        // Load Constantine translations
        $this->setup_translation();

        //Load Manifest
        $this->manifest = manifestController()->setup();

        //@todo Check for nessecary plugins
        $this->check_plugins();

        //Setup companion
        $this->setup_gf_companion();

        //Setup template controller
        templateController();

        //Setup Wrap Engine
        templateWrapController();

        //Setup page builder
        pageBuilder()->set_dynamic_views($this->manifest);

        //Setup page builder for dynamic views
        optionsPageFactory()->setup();

        //Setup page builder post types
        builderBlockFactory()->set_post_types($this->manifest->page_builder_for_post_types);

        //Setup fields
        fieldsFactory()->setup();

        //Setup block cpt
        if ($this->manifest->generic_blocks) {
            blockController();
        }

        //Setup cacheController
        cacheController();

        //Setup components
        $this->manifest->install_components();

        // dpr($this->manifest->loaded_components,2);

        if (array_has_items($this->manifest->loaded_components)) {
            foreach ($this->manifest->loaded_components as $component) {
                $component->installed();
            }
        }


        //Bootstrap theme builder
        if ($this->manifest->theme_builder) {
            bootstrapThemebuilder();
        }

        //Just dump auto load files on flush!
        add_filter('flush_rewrite_rules_hard', function ($bool) {
            $this->manifest->save_auto_load();
            return $bool;
        });

        // dpr($this->manifest->components);
        // dpr($this->manifest->loaded_components);

        //Setup theme
        add_action('after_setup_theme', array($this, 'setup_theme_support'));

        //Setup scripts
        add_action('wp_enqueue_scripts', array($this, 'setup_scripts'));

        //Setup styles
        add_action('wp_enqueue_scripts', array($this, 'setup_styles'));
    }

    /**
     * Setup the translations for constantineand theme
     *
     * @return void
     */
    public function setup_translation()
    {
        load_theme_textdomain('grafikfabriken', GF_ROOT . '/Languages');
    }


    /**
     * Setup defines
     *
     * @return void
     */
    private function setupDefines()
    {

        //Components
        if (!defined('GF_ROOT'))
            define('GF_ROOT', dirname(__FILE__, 2));

        if (!defined('GF_COMPONENTS_DIR'))
            define('GF_COMPONENTS_DIR', GF_ROOT . DIRECTORY_SEPARATOR . 'Components');

        if (!defined('GF_TEMPLATES'))
            define('GF_TEMPLATES', GF_ROOT . DIRECTORY_SEPARATOR . 'Templates');

        if (!defined('GF_TEMPLATES_WRAPPERS'))
            define('GF_TEMPLATES_WRAPPERS', GF_TEMPLATES . DIRECTORY_SEPARATOR . 'Wrappers');

        if (!defined('GF_TEMPLATES_PASSWORD_FORM'))
            define('GF_TEMPLATES_PASSWORD_FORM', GF_TEMPLATES . DIRECTORY_SEPARATOR . 'PasswordForm' . DIRECTORY_SEPARATOR . 'password-form.php');

        //Main Theme
        if (!defined('GF_THEME_ROOT_DIR'))
            define('GF_THEME_ROOT_DIR', dirname(__FILE__, 6));

        if (!defined('GF_THEME_BS_DIR'))
            define('GF_THEME_BS_DIR', GF_THEME_ROOT_DIR . DIRECTORY_SEPARATOR . 'node_modules/bootstrap-scss/');

        if (!defined('GF_THEME_BS_SELECT_DIR'))
            define('GF_THEME_BS_SELECT_DIR', GF_THEME_ROOT_DIR . DIRECTORY_SEPARATOR . 'node_modules/bootstrap-select/');

        if (!defined('GF_THEME_FONTAWESOME_DIR'))
            define('GF_THEME_FONTAWESOME_DIR', GF_THEME_ROOT_DIR . DIRECTORY_SEPARATOR . 'node_modules/@fortawesome/');

        if (!defined('GF_THEME_DIR'))
            define('GF_THEME_DIR', GF_THEME_ROOT_DIR . DIRECTORY_SEPARATOR . 'app');

        if (!defined('GF_THEME_COMPONENTS_DIR'))
            define('GF_THEME_COMPONENTS_DIR', GF_THEME_DIR . DIRECTORY_SEPARATOR . 'Components');

        if (!defined('GF_THEME_ASSETS_ENTRY_DIR'))
            define('GF_THEME_ASSETS_ENTRY_DIR', GF_THEME_DIR . DIRECTORY_SEPARATOR . 'assets');

        if (!defined('GF_THEME_ASSETS_ENTRY_STYLE_FILE'))
            define('GF_THEME_ASSETS_ENTRY_STYLE_FILE', GF_THEME_ASSETS_ENTRY_DIR . DIRECTORY_SEPARATOR . 'entry.scss');

        if (!defined('GF_THEME_ASSETS_CUSTOMIZER_STYLE'))
            define('GF_THEME_ASSETS_CUSTOMIZER_STYLE', GF_THEME_ASSETS_ENTRY_DIR . DIRECTORY_SEPARATOR . 'styles/_wp_customizer.scss');

        if (!defined('GF_THEME_ASSETS_DIR'))
            define('GF_THEME_ASSETS_DIR', GF_THEME_DIR . DIRECTORY_SEPARATOR . 'dist');

        if (!defined('GF_THEME_SCRIPTS_DIR'))
            define('GF_THEME_SCRIPTS_DIR', GF_THEME_ASSETS_DIR . DIRECTORY_SEPARATOR . 'scripts');

        if (!defined('GF_THEME_STYLES_DIR'))
            define('GF_THEME_STYLES_DIR', GF_THEME_ASSETS_DIR . DIRECTORY_SEPARATOR . 'css');

        if (!defined('GF_THEME_STYLES_FILE'))
            define('GF_THEME_STYLES_FILE', GF_THEME_STYLES_DIR . DIRECTORY_SEPARATOR . 'app.css');

        if (!defined('GF_THEME_URL'))
            define('GF_THEME_URL', get_stylesheet_directory_uri());

        if (!defined('GF_THEME_ASSETS_URL'))
            define('GF_THEME_ASSETS_URL', GF_THEME_URL . DIRECTORY_SEPARATOR . 'dist');

        if (!defined('GF_THEME_SCRIPTS_URL'))
            define('GF_THEME_SCRIPTS_URL', GF_THEME_ASSETS_URL . DIRECTORY_SEPARATOR . 'js');

        if (!defined('GF_THEME_STYLES_URL'))
            define('GF_THEME_STYLES_URL', GF_THEME_ASSETS_URL . DIRECTORY_SEPARATOR . 'css');

        if (!defined('GF_THEME_LANG_DIR'))
            define('GF_THEME_LANG_DIR', GF_THEME_DIR . DIRECTORY_SEPARATOR . 'lang');

        if (!defined('GF_THEME_MANIFEST'))
            define('GF_THEME_MANIFEST', GF_THEME_ROOT_DIR . DIRECTORY_SEPARATOR . 'manifest.yml');

        if (!defined('GF_THEME_AUTO_LOAD'))
            define('GF_THEME_AUTO_LOAD', GF_THEME_ROOT_DIR . DIRECTORY_SEPARATOR . 'autoload.json');

        if (!defined('GF_THEME_CACHE_FOLDER'))
            define('GF_THEME_CACHE_FOLDER', GF_THEME_DIR . DIRECTORY_SEPARATOR . 'cache');
    }

    /**
     * Check for nesscesary plugins
     *
     * @todo We need to implement this!
     * @return void
     */
    public function check_plugins()
    {
    }


    /**
     * Setup GF Compantion
     *
     * @return void
     */
    public function setup_gf_companion()
    {
        if (array_has_items($this->manifest->companion)) {
            foreach ($this->manifest->companion as $func) {
                gf_require($func);
            }
        }
    }

    /**
     * Setup theme support
     *
     * @return void
     */
    public function setup_theme_support()
    {
        if (array_has_items($this->manifest->theme_supports)) {
            foreach ($this->manifest->theme_supports as $key => $value) {
                if (array_has_items($value)) {
                    add_theme_support($key, $value);
                } else {
                    add_theme_support($key);
                }
            }
        }

        if ($this->manifest->text_domain != "") {
            load_theme_textdomain($this->manifest->text_domain, GF_THEME_LANG_DIR . '/lang');
        }
    }

    /**
     * Setup Javascript
     *
     * @return void
     */
    public function setup_scripts()
    {

        if (array_has_items($this->manifest->scripts)) {
            foreach ($this->manifest->scripts as $key => $value) {

                $url = String_Utils::is_href($value["file"]) ? $value["file"] : GF_THEME_SCRIPTS_URL . DIRECTORY_SEPARATOR . $value["file"];

                $deps = array_unique(apply_filters('gf_script_depdencies_' . $key, $value["deps"]));

                wp_enqueue_script($key, $url, $deps, $value["version"]);

                // Default values
                if (isset($value['localize']) && $value['localize'] == true) {
                    $array = array();
                    $array['ajax_url'] = admin_url('admin-ajax.php');

                    $localize = apply_filters('localize_script_' . $key, $array);
                    wp_localize_script($key, $key, $localize);
                }
            }
        }
    }

    /**
     * Setup styles
     *
     * @return void
     */
    public function setup_styles()
    {

        // Always remove WooCommerce style. Added 2019-02-19
        wp_dequeue_style('woocommerce-general');
        wp_dequeue_style('woocommerce-layout');
        wp_dequeue_style('woocommerce-smallscreen');

        if (array_has_items($this->manifest->styles)) {
            foreach ($this->manifest->styles as $key => $value) {
                $url = String_Utils::is_href($value["file"]) ? $value["file"] : GF_THEME_STYLES_URL . DIRECTORY_SEPARATOR . $value["file"];
                wp_enqueue_style($key, $url, $value["deps"], $value["version"]);
            }
        }
    }

    /**
     * Boot class
     *
     * @return void
     */
    public static function boot()
    {
        return self::getInstance();
    }
}


//REMOVE
if (!function_exists('gf_wp_allow_svg_uploads')) {
    function gf_wp_allow_svg_uploads($mimes)
    {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
    add_filter('upload_mimes', '\\GF\\Controllers\\gf_wp_allow_svg_uploads', 10, 1);
}
