<?php

namespace GF\Controllers;

use GF\Utils\Singleton;
use function GF\Utils\manifestController;
use function GF\Utils\cacheController;

class Template_Controller extends Singleton
{

    public $template_name = "template.php";

    /**
     * Generic WP Error
     *
     * @var \WP_Error
     */
    private $error;

    public function _construct()
    {
        $this->error = new \WP_Error();
    }


    public function display_error($name)
    {
        $this->error->add('gf_template_controller_not_found', "Could not found template: " . $name);
        wp_die($this->error);
    }
    /**
     * Get component!
     *
     * @param string $id
     * @return \GF\Models\Component
     */
    public function get_component($id)
    {
        return manifestController()->manifest->get_component_by_id($id);
    }


    /**
     * Install global sub components
     *
     * @param \GF\Models\Component $component
     * @return void
     */
    public function install_global_partials($component)
    {

        if (array_has_items($component->sub_components)) {
            foreach ($component->sub_components as $key => $sub_component) {

                if (is_a($component, '\GF\Models\Component')) {


                    $this->install_global_partials($sub_component);
                    $component->{$key} = $sub_component;
                    $sub_component->set_options();
                    $sub_component->before_render();
                }
            }
        }
    }

    /**
     * Get component by type
     *
     * @param string $type
     * @return \GF\Models\Component
     */
    public function get_component_by_type($type)
    {

        $component = manifestController()->manifest->get_component_by_type($type);

        $component->set_options();

        $this->install_global_partials($component);

        $component->before_render();

        return manifestController()->manifest->get_component_by_type($type);
    }

    /**
     * Render by type
     *
     * @param string $type
     * @return string
     */
    public function render_by_type($type)
    {

        $component = $this->get_component_by_type($type);

        if (is_a($component, '\\GF\\Models\\Component')) {

            $id = strtolower($component->id);
            $hash = cacheController()->generate_hash($id);
            if (cacheController()->exists($hash)) {
                return cacheController()->get_cache($hash);
            }

            $block_content = $component->render();

            if ($component->cache) {
                cacheController()->save_cache($hash, $block_content);
            }

            return $block_content;
        }

        return '';
    }

    /**
     * Get content
     *
     * @return void
     */
    public function get_content()
    {
        return '';
    }

    /**
     * Get base template path
     *
     * @return string
     */
    public function get_base_path()
    {
        return $this->get_component_by_type('Base')->get_template_path();
    }

    /**
     * Get theme header
     *
     * @return string
     */
    public function get_header()
    {
        return $this->render_by_type('Globals_Headers');
    }

    /**
     * Get theme preloader
     *
     * @return string
     */
    public function get_preloader()
    {
        return $this->render_by_type('Globals_Preloaders');
    }

    /**
     * Get theme outdated browser
     *
     * @return string
     */
    public function get_outdated_browser()
    {
        return $this->render_by_type('Globals_Outdatedbrowsers');
    }

    /**
     * Output
     *
     * @return void
     */
    public function get_footer()
    {
        return $this->render_by_type('Globals_Footers');
    }

    public function get_debug()
    {
        return $this->render_by_type('Globals_Debug');
    }
}
