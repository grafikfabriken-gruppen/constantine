<?php

namespace GF\Controllers;

use GF\Utils\Singleton;
use GF\Models\Builder_Block;
use function GF\Utils\builderBlockFactory;
use function GF\Utils\cacheController;

class Page_Builder_Controller extends Singleton
{
    /**
     * Acf page builder name
     *
     * @var string
     */
    public $name = "gf_page_builder";

    /**
     * Dynamic views is ovveriden archive, single post and so on
     *
     * @var array
     */
    public $dynamic_views;

    /**
     * Contains snappable objects
     *
     * @var array
     */
    public $snap_factory = [];


    /**
     * The section index
     *
     * @var int
     */
    public $section_index = 0;

    /**
     * Construct page builder
     */
    public function __construct()
    {
    }

    /**
     * Set dynamic views
     *
     * @param GF\Models\Manifest $views
     * @return void
     */
    public function set_dynamic_views($manifest)
    {
        $_views = $manifest->dynamic_views;
        $views = [];

        if (array_has_items($_views)) {
            foreach ($_views as $value) {
                $views[$value] = sprintf('page_builder_%s', $value);
            }
        }

        $this->dynamic_views = array_has_items($views) ? $views : array();
    }

    /**
     * Render the html from the builder
     *w
     * @param integer $id
     * @return void
     * @author Hampus Alstermo <hampus@grafikfabriken.nu>
     * @version 1.0
     */
    public function render($render = true, $id = 0)
    {
        global $post;
        /** @var \WP_Post $local_post */
        $local_post = $post;
        $html = '';
        $full_page_cache = false;
        $all_block_is_cached = true;
        $can_cache = (is_singular() || is_page()) ? true : false;
        $fphash = false;
        $real_cache_id = false;
        $fields = [];

        if ($id === 0) {
            $id = $post->ID;
        } elseif (is_int($id)) {
            $local_post = get_post($id);
            // Id means it's a single post and we can cache it
            $can_cache = true;
        }

        /**
         * Password form
         */
        if (is_int($id) && $id > 0 && \post_password_required()) {
            ob_start();
            require_once GF_TEMPLATES_PASSWORD_FORM;
            $html = \apply_filters('the_password_form', ob_get_clean());
            if ($render) {
                echo $html;
                return;
            } else {
                return $html;
            }
        }

        if (is_int($id) && $id > 0) {
            $real_cache_id = $id;
        } elseif (is_string($id)) {
            $real_cache_id = $id;
        }

        if (array_key_exists($id, $this->dynamic_views)) {
            // Fetch fields from settings page based on the view id
            $fields = get_field('gf_dynamic_page_builder_' . $id, "options");
        } elseif ($can_cache) {
            $full_page_cache = get_post_meta($real_cache_id, '_full_page_cache', true) == 1 ? true : false;

            $fpid = "full_page_cache_" . $real_cache_id;
            $fphash = cacheController()->generate_hash($fpid);

            if ($full_page_cache && cacheController()->exists($fphash)) {
                if ($render) {
                    echo cacheController()->get_cache($fphash);
                    return;
                } else {
                    return cacheController()->get_cache($fphash);
                }
            }

            $fields = get_field($this->name, $id);
        }

        if (array_has_items($fields)) {
            foreach ($fields as $key => $field) {

                $chain_id = isset($field["acf_fc_layout"]) ? $key . "_" . $field["acf_fc_layout"] : $key;
                $hash = cacheController()->generate_hash($chain_id);

                if (cacheController()->exists($hash)) {

                    $cache_html = cacheController()->get_cache($hash);
                    $html .= $cache_html;
                    if (strpos($cache_html, 'snappable')) {
                        $this->snap_factory[$this->section_index] = true;
                    } else {
                        $this->snap_factory[$this->section_index] = false;
                    }

                    if (strpos($cache_html, 'section-index')) {
                        $this->section_index++;
                    }

                } else {
                    if ($block = builderBlockFactory()->get_block_by_name($field)) {

                        // Change the path after the components are installed if the parent templates should be used
                        if ($block->use_parent_template) {
                            $block->path = $block->get_component_dir();
                        }

                        // Map the dynamic data if it's a dynamic block
                        if ($block->is_dynamic()) {
                            $block->install_sub_components();

                            // Send in the queried object (custom model) for mapping in the component
                            if (is_singular()) {
                                $block->map_dynamic_data(get_custom_model(get_queried_object()));
                            } else {
                                $block->map_dynamic_data(get_queried_object());
                            }
                        }

                        $block->before_render();

                        //Handle snap
                        if (is_a($block, '\GF\Models\Component_PB_Block')) {

                            if ($block->snap) {
                                if (array_has_items($this->snap_factory) && isset($this->snap_factory[$this->section_index - 1]) && $this->snap_factory[$this->section_index - 1]) {
                                    $block->snapped = true;
                                }
                            }

                            //Add section index css classs
                            if ($block->use_section_index) {
                                $block->css .= ' section-index';
                            }
                        }

                        $block_content = $block->render();

                        // Do not snapp if content is empty
                        if (is_a($block, '\GF\Models\Component_PB_Block') && $block_content != "") {
                            if ($block->use_section_index) {
                                $this->snap_factory[$this->section_index] = $block->snappable;
                                $this->section_index++;
                            }
                        }

                        //Dont cache if content is empty
                        if ($block->cache && $block_content != "") {
                            cacheController()->save_cache($hash, $block_content);
                        } else {
                            $all_block_is_cached = false;
                        }

                        $html .= $block_content;
                    }
                }

            }


            /**
             * Maybe update cache
             * 
             */
            if ($real_cache_id != null && $fphash) {
                if ($all_block_is_cached) {
                    update_post_meta($real_cache_id, '_full_page_cache', true);
                    cacheController()->save_cache($fphash, $html);
                } else {
                    update_post_meta($real_cache_id, '_full_page_cache', false);
                }
            }
        }

        if ($render) {
            echo $html;
        } else {
            return $html;
        }
    }
}
