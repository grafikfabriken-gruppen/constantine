<?php
namespace GF\Controllers;

use GF\Utils\Singleton;
use function GF\Utils\templateController;

class Template_Wrap_Controller extends Singleton{

    /**
     * Main template
     *
     * @var string
     */
    public $main_template;

    /**
     * Base template
     *
     * @var string
     */
    public $base_template;

    /**
     * Slug
     *
     * @var string
     */
    public $slug;

    /**
     * Templates
     *
     * @var array
     */
    public $templates;

    /**
     * Add hooks for template wrapping
     *
     * @return void
     */
    public function _construct(){
        add_filter('template_include', array($this, 'display_base'), 11);
    }

    /**
     * Get requested page template
     *
     * @param string|array $template
     * @return string|array
     */
    public function display_base($template){

        //If someone else modified our theme
        if (!is_string($template)) {
            return $template;
        }

        $this->main_template = $template;
        $this->base_template = basename($template, '.php');

        //If we have an other page in our theme folder!
        if ($this->base_template === 'index') {
            $this->base_template = false;
        }

        //Let us decide wich template
        return $this->build_template_alternatives();

    }

    /**
     * This checks if we have alternative base templates
     *
     * @param string $template
     * @return void
     */
    public function build_template_alternatives($template = 'base.php')
    {
        
        $this->slug = strpos($template, '.php') === false ? basename($template, '.php') : $template;
        $this->templates = [$template];

        if ($this->base_template) {
            $str = substr($template, 0, -4);
            // dpr($this->base_template);
            array_unshift($this->templates, sprintf($str . '-%s.php', $this->base_template));
        }
        return $this;
    }

    /**
     * Gotta love magic methods!
     *
     * @return string
     */
    public function __toString()
    {
        $templates = locate_template($this->templates);
        return $templates ? $templates : templateController()->get_base_path();
    }

    /**
     * Get main template
     *
     * @return string
     */
    public function get_main_template(){
        return $this->main_template;
    }

}