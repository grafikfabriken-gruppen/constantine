## Constantine 1.1.0

Begun to add support for SEO on custom fields

## Constantine 1.0.4

Constantine is an template engine for grafikfabriken/theme.
The main purpose is to have an repository for all of our components.
So we can build ultra-fast wordpress sites!

## For updates

run `composer update`